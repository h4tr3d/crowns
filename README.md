# Crowns

Приложение для графического построения пробных площадей и замеров на них. Идея создания программы принадлежит Возмищевой Анне, основана на идеях воплощенных в программе Lepeshki, написанной Александром Омелько (Delphi, Windows). При создании делался упор на возможность кроссплатформенного использования приложения, устранение некоторых ограничений, реализация дополнительного функционала.

Используюя данное приложение, вы можете:

- Загружать данные по замерам на пробных площадях и делать их отрисовку;
- Манипулировать параметрами отрисовки;
- Делать замеры дистанций и площадей;
- Создавать срезы;
- Экспортировать результаты построений в графические форматы;
- Сохранять и загружать проекты, экспортировать части проекта для последующего использования.


Детальная информация:
- [Справочное руководство](/doc/crowns.md)
- [Снимки экрана](/doc/screenshots.md)
- [Скачать](https://gitlab.com/h4tr3d/crowns/tags)
