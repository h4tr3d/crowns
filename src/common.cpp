#include "common.h"
#include <iostream>

bool sortDataCompareHeight(DataRec &rec1, DataRec &rec2)
{
    // Функция сравнения для алгоритма сортировки по высоте
    // для работы с функцией qSort

    return (rec1.H < rec2.H);
}

bool sortDataCompareDiameter(DataRec &rec1, DataRec &rec2)
{
    // Функция сравнения для алгоритма сортировки по диаметру
    // для работы с функцией qSort

    return (rec1.D < rec2.D);
}

bool sortDataCompareTreeNum(DataRec &rec1, DataRec &rec2)
{
    // Функция сравнения для алгоритма сортировки по диаметру
    // для работы с функцией qSort

    return (rec1.TreeNum < rec2.TreeNum);
}

bool sortDataCompareYCoord(DataRec &rec1, DataRec &rec2)
{
    // Функция сравнения для алгоритма сортировки по Y координате
    // для работы с функцией qSort

    return (rec1.Y < rec2.Y);
}

bool sortSetsByType(SetRec &rec1, SetRec &rec2)
{
    // Функция сравнения для алгоритма сортировки по диаметру
    // для работы с функцией qSort

    return (rec1.code < rec2.code);
}

void setRecFillDefault(SetRec *rec)
{
    // Заполняет структуру с параметрами породы умолчательными значениями
    rec->code = -1;
    rec->trunkColor = Qt::black;
    rec->projLineColor = Qt::magenta;
    rec->projLineStyle = Qt::SolidLine;
    rec->projFillColor = Qt::green;
    rec->projLineWidth = 1;
    rec->spName = QString("No name");
    rec->draw = true;
    rec->isFill = false;
    rec->spType = 1;
    rec->projFillStyle = 1;
}

QPainterPath piePath(QRectF rect, float startA, float spanA)
{
    QPainterPath path;
    QPointF start(rect.x() + rect.width()/2.0, rect.y() + rect.height()/2.0);
    
    path.moveTo(start);
    path.arcTo(rect, startA, spanA);
    path.closeSubpath();
    
    return path;
}

QPainterPath arcPath(QRectF rect, float startA, float spanA)
{
    QPainterPath path;

    float a, b, k, x, y;
    
    a = rect.width() / 2.0;
    b = rect.height()  / 2.0;
    k = tan(startA * M_PI / 180.0);
    
    x = sqrt(1.0 / (1.0 / (a*a) + (k*k) / (b*b)));

    if( (startA > 90 && startA < 270) ||
        (startA < -90 && startA > -270))
    {
        x = -x;    
    }
    
    y = -k * x;
    
    if( (startA > 180 && startA < 360) ||
        (startA <-180 && startA >-360))
    {
        y = -y;    
    }
    
    x += rect.x() + a;
    y += rect.y() + b;

    std::cout << "X = " << x << " Y = " << y << " a = " << a << " b = " << b << " k = " << k << std::endl;
    std::cout << "startA = " << startA << " spanA = " << spanA << std::endl;    
    
    QPointF start(x, y);
    
    path.moveTo(start);
    path.arcTo(rect, startA, spanA);
    
    return path;
}

QPainterPath chordPath(QRectF rect, float startA, float spanA)
{
    QPainterPath path;

    path = arcPath(rect, startA, spanA);
    path.closeSubpath();
    
    return path;
}

void MathPrepare(DataRec *rec)
{
    //float Ny = rec->Ny;
    //float Sy = rec->Sy;
    //float Wx = rec->Wx;
    //float Ex = rec->Ex;
    
    float DX, DY;
    
    //if(Ny == 0) Ny = 1.0;
    //if(Sy == 0) Sy = 1.0;
    //if(Wx == 0) Wx = 1.0;
    //if(Ex == 0) Ex = 1.0;
    if (rec->Ny == 0) rec->Ny = 0.1;
    if (rec->Sy == 0) rec->Sy = 0.1;
    if (rec->Wx == 0) rec->Wx = 0.1;
    if (rec->Ex == 0) rec->Ex = 0.1;
    if (fabs(rec->Ny) < 0.3) rec->Ny *= 2.0;
    if (fabs(rec->Sy) < 0.3) rec->Sy *= 2.0;
    if (fabs(rec->Wx) < 0.3) rec->Wx *= 2.0;
    if (fabs(rec->Ex) < 0.3) rec->Ex *= 2.0;

    DX = rec->Wx + rec->Ex;
    DY = rec->Ny + rec->Sy;

    if (rec->Ny < 0)
    {
        rec->Wy = fabs(rec->Ny) + 1.0 / 2.0 * DY;
        rec->Ey = fabs(rec->Ny) + 1.0 / 2.0 * DY;
    }
    if (rec->Sy < 0)
    {
        rec->Wy = -1.0 * (fabs(rec->Sy) + 1.0 / 2.0 * DY);
        rec->Ey = -1.0 * (fabs(rec->Sy) + 1.0 / 2.0 * DY);
    }

    if (rec->Wx < 0)
    {
        rec->Nx = fabs(rec->Wx) + 1.0 / 2.0 * DX;
        rec->Sx = fabs(rec->Wx) + 1.0 / 2.0 * DX;
    }
    if (rec->Ex < 0)
    {
        rec->Nx = -1.0 * (fabs(rec->Ex) + 1.0 / 2.0 * DX);
        rec->Sx = -1.0 * (fabs(rec->Ex) + 1.0 / 2.0 * DX);
    }
}

/**
 * Расчет параметров кровой Безье
 * @param xy0      - X или Y координата начала кривой
 * @param xy_ctrl0 - X или Y координата первой контрольной точки
 * @param xy_ctrl1 - X или Y координата второй контрольной точки
 * @param xy1      - X или Y координата конца кривой
 * @param t        - параметр кривой в пределах [0..1] описывающий движение по кривой
 *                   (0 - начало кривой, 1 - конец), правильный шаг обеспечивает "гладкость"
 *                   кривой, в большинстве случаев шаг 0.1 бывает достаточным
 * @return значение X или Y координаты для текущего t
 */
qreal getBezierXY(qreal xy0, qreal xy_ctrl0, qreal xy_ctrl1, qreal xy1, qreal t)
{
    // Возвращает координату по параметру кривой Безье
    qreal r;
    r = pow((1 - t), 3) * xy0 + 3 * pow((1 - t), 2) * t * xy_ctrl0 + 3 * (1 - t) * pow(t, 2) * xy_ctrl1 + pow(t, 3) * xy1;
    return r;
}
