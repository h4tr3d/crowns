#include <QApplication>
#include "mainimpl.h"
//

#include "main.h"

//MainImpl win;

int main(int argc, char ** argv)
{
	QApplication app( argc, argv );
	MainImpl win;
	win.show(); 
	app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
	return app.exec();
}
