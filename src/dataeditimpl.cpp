#include "dataeditimpl.h"
//
DataEditImpl::DataEditImpl(  QWidget * parent, Qt::WindowFlags f  ) 
    : QDialog( parent, f )
{
    setupUi(this);
    // TODO
    
    DataRec tmp;
    memset(&tmp, 0, sizeof(tmp));
    setDataItem(tmp);
    
    sets.clear();
}
//

void DataEditImpl::setDataItem(DataRec &value)
{
    dataItem = value;
    
    lineEdit_TreeNum->setText(QString("%1").arg(value.TreeNum));
    spin_X->setValue(value.X);
    spin_Y->setValue(value.Y);
    spin_Nx->setValue(value.Nx);
    spin_Ny->setValue(value.Ny);
    spin_Sx->setValue(value.Sx);
    spin_Sy->setValue(value.Sy);
    spin_Wx->setValue(value.Wx);
    spin_Wy->setValue(value.Wy);
    spin_Ex->setValue(value.Ex);
    spin_Ey->setValue(value.Ey);
    spin_D->setValue(value.D);
    spin_H->setValue(value.H);
    spin_A->setValue(value.age);
    spin_spCode->setValue(value.spCode);
}

//
void DataEditImpl::on_lineEdit_TreeNum_textChanged(QString text)
{
    // Устанавливает номер дерева
    dataItem.TreeNum = text.toInt();
}

void DataEditImpl::on_spin_X_valueChanged(double v)
{
    dataItem.X = v;
}

void DataEditImpl::on_spin_Y_valueChanged(double v)
{
    dataItem.Y = v;
}

void DataEditImpl::on_spin_Nx_valueChanged(double v)
{
    dataItem.Nx = v;
}

void DataEditImpl::on_spin_Ny_valueChanged(double v)
{
    dataItem.Ny = v;
}

void DataEditImpl::on_spin_Sx_valueChanged(double v)
{
    dataItem.Sx = v;
}

void DataEditImpl::on_spin_Sy_valueChanged(double v)
{
    dataItem.Sy = v;
}

void DataEditImpl::on_spin_Wx_valueChanged(double v)
{
    dataItem.Wx = v;
}

void DataEditImpl::on_spin_Wy_valueChanged(double v)
{
    dataItem.Wy = v;
}

void DataEditImpl::on_spin_Ex_valueChanged(double v)
{
    dataItem.Ex = v;
}

void DataEditImpl::on_spin_Ey_valueChanged(double v)
{
    dataItem.Ey = v;
}

void DataEditImpl::on_spin_D_valueChanged(double v)
{
    dataItem.D = v;
}

void DataEditImpl::on_spin_H_valueChanged(double v)
{
    dataItem.H = v;
}

void DataEditImpl::on_spin_spCode_valueChanged(int v)
{
    dataItem.spCode = v;
}

void DataEditImpl::on_spin_A_valueChanged(int v)
{
    dataItem.age = v;
}

void DataEditImpl::on_buttonBox_accepted()
{
    done(1);
}

void DataEditImpl::on_buttonBox_rejected()
{
    done(0);
}

void DataEditImpl::setSets(QList<SetRec> s)
{
    sets = s;
}


