#include "areameasure.h"
#include "common.h"
//
AreaMeasure::AreaMeasure( QGraphicsScene *s ) 
{
	// TODO
    scene = s;
    pathItem = NULL;
    pathControlItem = NULL;
    draw_line = NULL;
    isEnd = false;
    scale = 1;
    areaSize = 0;
    accuracy = 0.1;
}
//

void AreaMeasure::addBasePoint(QPointF p)
{
    // TODO
    if(isEnd) return;
    
    basePoints << p;
    std::cout << "Count of points: " << basePoints.size() << std::endl;
    
    if (basePoints.size() >= 2)
    {
        QPointF c1;
        QPointF c2;
        
        int i = basePoints.size() - 1;
        
        c1.setX(basePoints.at(i - 1).x() + (basePoints.at(i).x() - basePoints.at(i - 1).x())/2.0);
        c1.setY(basePoints.at(i - 1).y() + (basePoints.at(i).y() - basePoints.at(i - 1).y())/2.0);
        c2 = c1;
        
        pathPoints << c1 << c2;
    }
    
    pathPoints << p;
}


void AreaMeasure::redraw()
{
    int i;
    QGraphicsEllipseItem *el;

    if(pathPoints.size() < 4) return;    

    clear();    

    for(i = 0; i < pathPoints.size(); i++)
    {        
        el = scene->addEllipse(pathPoints.at(i).x() - ctrlSize/2,
                               pathPoints.at(i).y() - ctrlSize/2,
                               ctrlSize, ctrlSize);
        //el->setFlag(QGraphicsItem::ItemIsMovable, true);
        el->setZValue(10000);
        controlItem << el;
    }
    
    update();
    calcArea();
}


void AreaMeasure::clear()
{
    // TODO
    if(pathItem != NULL)
        scene->removeItem(pathItem);
        
    if(pathControlItem != NULL)
        scene->removeItem(pathControlItem);
     
    for(int i = 0; i < controlItem.size(); i++)
    {
        scene->removeItem(controlItem.at(i));
    }
    
    controlItem.clear();
    pathItem = NULL;
    pathControlItem = NULL;
}


void AreaMeasure::updatePointByIndex(int index, QPointF p)
{
    // TODO
    if(index < 0 || index >= pathPoints.size())
        return;
 
    if(pathPoints[index] != p)
    {
        pathPoints[index] = p;
        redraw();
    }
}

void AreaMeasure::update()
{
    // TODO
    int i;
    QPainterPath path;
    QPainterPath pathControl;
    if(pathPoints.size() < 4) return;    

    if(pathItem != NULL)
        scene->removeItem(pathItem);

    if(pathControlItem != NULL)
        scene->removeItem(pathControlItem);    
    
    path.moveTo(pathPoints.at(0));
    pathControl.moveTo(pathPoints.at(0));

    for(i = 1; i < pathPoints.size(); i+=3)
    {
        QPointF endPoint;
        if(isEnd && i == pathPoints.size() - 2)
        {
            endPoint = pathPoints.at(0);    
        }
        else
        {
            endPoint = pathPoints.at(i + 2);
        }
        
        path.cubicTo(pathPoints.at(i),
                     pathPoints.at(i + 1),
                     endPoint);
        
        pathControl.lineTo(pathPoints.at(i));
        pathControl.lineTo(pathPoints.at(i + 1));
        pathControl.lineTo(endPoint);
    }
    
    pathItem = scene->addPath(path);
    pathItem->setZValue(9000);
    
    pathControlItem = scene->addPath(pathControl);
    pathControlItem->setZValue(9000);
}


void AreaMeasure::start()
{
    clear();
    isEnd = false;
    areaSize = 0;
    basePoints.clear();
    pathPoints.clear();
}

void AreaMeasure::end()
{
    // Закончить рисовать площадь
    isEnd = true;
    
    QPointF c1;
    QPointF c2;
    
    int i = basePoints.size() - 1;
    
    c1.setX(basePoints.at(i).x() + (basePoints.at(0).x() - basePoints.at(i).x())/2.0);
    c1.setY(basePoints.at(i).y() + (basePoints.at(0).y() - basePoints.at(i).y())/2.0);
    c2 = c1;
    
    pathPoints << c1 << c2;

    if(draw_line != NULL)
    {
        scene->removeItem(draw_line);
        draw_line = NULL;
    }
    
    redraw();
}


void AreaMeasure::updatePoint(QPointF pos, UpdatePointStatus s)
{
	// Обновление точки по её приближенным координатам, полезна для работы с мышью
    int i;
    static qreal xm = 0, ym = 0;
    static bool is_move = false;
    static int idx = 0;
    static QPointF oldPos;

    switch(s)
    {
        case StartMovePoint:
                xm = pos.x();
                ym = pos.y();
                
                for(i=0; i < pathPoints.size(); i++)
                {
                    qreal x, y;
                    
                    x = pathPoints.at(i).x();
                    y = pathPoints.at(i).y();
                    
                    if(xm >= (x - ctrlSize / 2) &&
                       xm <= (x + ctrlSize / 2) &&
                       ym >= (y - ctrlSize / 2) &&
                       ym <= (y + ctrlSize / 2))
                    {
                        oldPos = pathPoints[i];
                        pathPoints[i] = pos;
                        xm = pos.x();
                        ym = pos.y();
                        idx = i;
                        is_move = true;
                        break;
                    }
                }
            break;
        case ProcessMovePoint:
                if(is_move == true)
                {
                    pathPoints[idx] = pos;    
                }
            break;
        case StopMovePoint:
                is_move = false;
            break;
    }
    
    if(is_move && oldPos != pos)
        redraw();
    
    oldPos = pos;
}

void AreaMeasure::calcArea()
{
    // Считаем площадь фигуры по приближенному методу: разбивка н-угольник с
    // заданной точностью (accuracy)
    
    int n  = basePoints.size();
    qreal n2 = 1.0/accuracy * n;
    int i, j;
    qreal k;

    QVector<QPointF> curve;

    if(!isEnd)
        return;    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    // координаты
    //int tmp=0;
    for(i = 1, j = 0; i < pathPoints.size(); i+=3, j++)
    {
        QPointF endPoint;
        QPointF startPoint;
        QPointF c1Point;
        QPointF c2Point;
        
        if(isEnd && i == pathPoints.size() - 2)
        {
            endPoint = pathPoints.at(0);    
        }
        else
        {
            endPoint = pathPoints.at(i + 2);
        }

        startPoint = pathPoints.at(i - 1);
        c1Point    = pathPoints.at(i);
        c2Point    = pathPoints.at(i + 1);
        
        if(j == 0)
        {
            curve << startPoint;
            //tmp++;
        }
        
        for(k = accuracy; k < 1.0; k+=accuracy)
        {
            curve << QPointF(
                     getBezierXY(startPoint.x(), c1Point.x(), c2Point.x(), endPoint.x(), k),
                     getBezierXY(startPoint.y(), c1Point.y(), c2Point.y(), endPoint.y(), k));
            //tmp++;
            //std::cout << "k = " << k << ", ";
        }
        
        if(j != (n-1))
        {
            curve << endPoint;
            //tmp++;
        }
        
        //std::cout << std::endl << "j = " << j << ", ";
    }
    
    //std::cout << std::endl;
    //std::cout << "Points: " << curve.size() << " / " << n2 << " " << tmp << std::endl;

    //return;
    // считаем площадь
    qreal s = 0;
    qreal res = 0;    
    for(i = 0; i < curve.size(); i++)
    {
        if(i == 0)
        {
            s = curve[i].x() * (curve[curve.size() - 1].y() - curve[i + 1].y());
            res += s;
        }
        else
        {
            if(i == curve.size()-1)
            {
                s = curve[i].x() * (curve[i - 1].y() - curve[0].y());
                res += s;
            }
            else
            {
                s = curve[i].x() * (curve[i - 1].y() - curve[i + 1].y());
                res += s;
            }
        }
    }
    
    areaSize = fabs(res/2);
    //std::cout << "Area size: " << areaSize << std::endl;
    
    areaSize = areaSize / pow(scale, 2);
}

void AreaMeasure::drawLine(QPointF p)
{
    // TODO
    if(draw_line != NULL)
    {
        scene->removeItem(draw_line);
        draw_line = NULL;
    }
    
    QPointF start;
    
    if(basePoints.size() > 0)
    {
        start = basePoints.at(basePoints.size() - 1);
        draw_line = scene->addLine(QLineF(start, p));    
    }
}

