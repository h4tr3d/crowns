#ifndef MAINIMPL_H
#define MAINIMPL_H
//
#include <QDialog>
#include <QMenuBar>
#include <QFileDialog>
#include <QTextStream>
#include <QtAlgorithms>
#include <QColorDialog>
#include <QFontDialog>
#include <QTransform>
#include <QMessageBox>
#include <QShortcut>
#include <QSettings>
#include <QGraphicsTextItem>
#include <QCloseEvent>
#include <QSvgGenerator>
#include <QDebug>

#include <math.h>
#include <iostream>

#include "ui_main.h"
#include "common.h"
#include "mygraphicsscene.h"
#include "customevent.h"
#include "palleteeditimpl.h"
#include "dataeditimpl.h"
#include "logger.h"
#include "areameasure.h"
#include "helpbrowser.h"
//
typedef enum {
    StateNone,
    StateMeasureArea,
    StateMeasureAreaDraw,
    StateMeasureDistance,
    StateCut
} AppStates;
//
class MainImpl : public QWidget, public Ui::Main
{
Q_OBJECT
public slots:

protected:
    void closeEvent(QCloseEvent * ev);
    bool event(QEvent *ev);

public:
    Logger log;
    void setCutDeep(qreal value)
    {
        cutDeep = value;
        spinBox_CutDeep->setValue(value);
    }
    qreal getCutDeep() { return cutDeep; }
    void setTransVal(int value)
    {
        transVal = value;
        spinBox_Transparent->setValue(value);
    }
    int getTransVal() { return transVal; }
    void drawBorderMarks(QGraphicsScene *s, float w, float h, float shiftP, int flag);
    void setCutCoord(QPointF value) { cutCoord = value; }
    QPointF getCutCoord() { return cutCoord; }
    void setMeasureEnd(QPointF value) { measureEnd = value; }
    QPointF getMeasureEnd() { return measureEnd; }
    void setMeasureStart(QPointF value) { measureStart = value; }
    QPointF getMeasureStart() { return measureStart; }
    
    SetRec getSetRec(int spCode);
    
    void setScaleFactor(int value)
    {
        scaleFactor = value;
        lineEdit_ScaleFactor->setText(QString("%1").arg(value));
        
        area->setScale((qreal)value);
    }
    
    int getScaleFactor()
    {
        return scaleFactor;
    }
    
    void setCrownNodesSize(int value)
    {
        crownNodesSize = value;
        lineEdit_CrownNodesSize->setText(QString("%1").arg(value));
    }

    int getCrownNodesSize()
    {
        return crownNodesSize;
    }

    void setIsCrownNodesShow(bool value)
    {
        isCrownNodesShow = value;
        checkBox_CrownNodesShow->setChecked(value);
    }
    
    bool getIsCrownNodesShow()
    {
        return isCrownNodesShow;
    }
    
    void setTrunkFont(QFont value)
    {
        trunkFont = value;
        label_TrunkFontShow->setFont(value);
        label_TrunkFontShow->setText(value.toString());
    }

    QFont getTrunkFont()
    {
        return trunkFont;
    }

    void setIsTrunkNumerationShow(bool value)
    {
        isTrunkNumerationShow = value;
        checkBox_TrunkNumerationShow->setChecked(value);
    }
    
    bool getIsTrunkNumerationShow()
    {
        return isTrunkNumerationShow;
    }
    
    void setTrunkDiameterTypoSize(int value)
    {
        trunkDiameterTypoSize = value;
        lineEdit_TrunkDiameterTypical->setText(QString("%1").arg(value));
    }

    int getTrunkDiameterTypoSize()
    {
        return trunkDiameterTypoSize;
    }

    void setTrunkDiameter(TrunkDiameter value)
    {
        trunkDiameter = value;
        switch(value)
        {
            case TRUNK_DIAMETER_REAL:
            {
                radioButton_TrunkDiameterReal->setChecked(true);
                break;
            }
            
            case TRUNK_DIAMETER_TYPO:
            {
                radioButton_TrunkDiameterTypical->setChecked(true);
                break;
            }
        }

    }

    TrunkDiameter getTrunkDiameter()
    {
        return trunkDiameter;
    }

    void setTrunkView(TrunkView value)
    {
        trunkView = value;
        switch(value)
        {
            case TRUNK_VIEW_CIRCLE:
            {
                radioButton_TrunkViewCircle->setChecked(true);
                break;
            }
            
            case TRUNK_VIEW_PALLETE:
            {
                radioButton_TrunkViewPallete->setChecked(true);
                break;
            }
        }
    }

    TrunkView getTrunkView()
    {
        return trunkView;
    }

    void setColorGridInter(QColor value)
    {
        colorGridInter = value;
        int r, g, b, a;
        value.getRgb(&r, &g, &b, &a);
        button_ColorGridInter->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
    }
    
    QColor getColorGridInter()
    {
        return colorGridInter;
    }
    void setColorGridMain(QColor value)
    {
        colorGridMain = value;
        int r, g, b, a;
        value.getRgb(&r, &g, &b, &a);
        button_ColorGridMain->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
    }
    
    QColor getColorGridMain()
    {
        return colorGridMain;
    }
    
    void setGridInterStep(int value)
    {
        gridInterStep = value;
        lineEdit_GridInterStep->setText(QString("%1").arg(value));
    }
    
    int getGridInterStep()
    {
        return gridInterStep;
    }
    
    void setGridMainStep(int value)
    {
        gridMainStep = value;
        lineEdit_GridMainStep->setText(QString("%1").arg(value));
    }
    
    int getGridMainStep()
    {
        return gridMainStep;
    }
    
    void setIsGridShow(bool value)
    {
        isGridShow = value;
        checkBox_GridShow->setChecked(value);
    }
    
    bool getIsGridShow()
    {
        return isGridShow;
    }
    
    void setMarksFont(QFont value)
    {
        marksFont = value;
        label_MarksFontShow->setFont(value);
        label_MarksFontShow->setText(value.toString());
    }

    QFont getMarksFont()
    {
        return marksFont;
    }
    
    void setIsMarksNumeration(bool value)
    {
        isMarksNumeration = value;
        checkBox_MarksNumeration->setChecked(value);
    }
    
    bool getIsMarksNumeration()
    {
        return isMarksNumeration;
    }
    
    void setMarksStep(int value)
    {
        marksStep = value;
        lineEdit_MarksStep->setText(QString("%1").arg(value));
    }
    
    int getMarksStep()
    {
        return marksStep;
    }
    
    void setIsMarksShow(bool value)
    {
        isMarksShow = value;
        checkBox_MarksShow->setChecked(value);
    }
    
    bool getIsMarksShow()
    {
        return isMarksShow;
    }
    
    void setColorPlateBorder(QColor value)
    {
        colorPlateBorder = value;
        int r, g, b, a;
        value.getRgb(&r, &g, &b, &a);
        button_ColorPlateBorder->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
    }
    
    QColor getColorPlateBorder()
    {
        return colorPlateBorder;
    }

    void setBorderWidth(int value)
    {
        borderWidth = value;
        lineEdit_BorderWidth->setText(QString("%1").arg(value));
    }
    
    int getBorderWidth()
    {
        return borderWidth;
    }
    
    void setIsPlateBorderDisplay(bool value)
    {
        isPlateBorderDisplay = value;
        checkBox_BorderDisplay->setChecked(value);
    }
    
    bool getIsPlateBorderDisplay()
    {
        return isPlateBorderDisplay;
    }
    
    void setIsClearCrowns(bool value)
    {
        isClearCrowns = value;
        checkBox_ClearCrowns->setChecked(value);
    }

    bool getIsClearCrowns()
    {
        return isClearCrowns;
    }

    void setPlateShapeWidth(int value)
    {
        plateShapeWidth = value;
        lineEdit_PlateShapeWidth->setText(QString("%1").arg(value));
    }
    
    int getPlateShapeWidth()
    {
        return plateShapeWidth;
    }
    
    void setPlateHeight(int value)
    {
        plateHeight = value;
        lineEdit_PlateHeight->setText(QString("%1").arg(value));
    }
    
    int getPlateHeight()
    {
        return plateHeight;
    }
    
    void setPlateWidth(int value)
    {
        plateWidth = value;
        lineEdit_PlateWidth->setText(QString("%1").arg(value));
    }
    
    int getPlateWidth()
    {
        return plateWidth;
    }

    void setProbePlateColor(QColor value)
    {
        probePlateColor = value;
        int r, g, b, a;
        value.getRgb(&r, &g, &b, &a);
        button_ColorPlate->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
    }

    QColor getProbePlateColor()
    {
        return probePlateColor;
    }
    
    RestValues getRestValues();
    uint getRestrictions();
    uint getSortType();
    uint getLayouts();
    MainImpl( QWidget * parent = 0, Qt::WindowFlags f = Qt::Widget );

private:
    void exportToPng(QString file_name, QGraphicsScene *s, int width, int height);
    void exportToSvg(QString file_name, QGraphicsScene *s, int width, int height);
    void exportToImg(QGraphicsScene *s, int width, int height);

private:
    AreaMeasure *area;
    int appState;
    bool projSaved;
    QString projFileName;
    qreal cutDeep;
    int transVal;
    qreal topZValue;
    qreal zValue;
    float cutH;
    float cutW;
    QPointF cutCoord;
    QPointF measureEnd;
    QPointF measureStart;
    MouseState mouseState;
    QList<int> TreeNums;
    int crownNodesSize;
    bool isCrownNodesShow;
    QFont trunkFont;
    bool isTrunkNumerationShow;
    int trunkDiameterTypoSize;
    TrunkDiameter trunkDiameter;
    TrunkView trunkView;
    QColor colorGridInter;
    QColor colorGridMain;
    int gridInterStep;
    int gridMainStep;
    bool isGridShow;
    QFont marksFont;
    bool isMarksNumeration;
    int marksStep;
    bool isMarksShow;
    QColor colorPlateBorder;
    int borderWidth;
    bool isPlateBorderDisplay;
    bool isClearCrowns;
    int plateShapeWidth;
    int plateHeight;
    int plateWidth;
    QColor probePlateColor;

    void measureActions(int state, bool checked);
    void customEvent(QEvent *ev);
    void setAppState(int stateNew);
    void setTitle(QString str);
    int checkProjectSave();
    int importProjectFromIniFile(QString fileName);
    int exportProjectToIniFile(QString fileName);

    void setDefaults();
    void setRestValues(RestValues val);
    void setRestrictions(uint val);
    void setSortType(SortType val);
    void setLayouts(uint val);
    QString genTreeToolTip(DataRec dat, SetRec set);
    void drawGrid(QGraphicsScene *s, float w, float h, float shiftP);
    void drawCut();

    int exportDataToIniFile(QString fileName);
    int exportDataToTxtFile(QString fileName, TxtDataFormat format);

    void importDataFromIniFile(QString fileName);
    void clearTable(QTableWidget *table);
    void addSetDataItem(int index, DataRec rec);
    int importPalleteFromIniFile(QString fileName);
    int exportPalleteToIniFile(QString fileName);
    void addSetPalleteItem(int index, SetRec set);
    int scaleFactor;
    void fillMenu();
    void sortData();
    void drawPlotBase(QGraphicsScene *s, int shiftP, int scale, int w, int h, int stage);
    void drawPlot();
    void importDataFromTxtFile(QString fileName, TxtDataFormat format);
    void drawCrownPart(QGraphicsScene *s, DataRec dRec, SetRec rec, QPointF P1, QPointF P2, qreal X1, qreal Y1, qreal X2, qreal Y2);
    void drawCrown(QGraphicsScene *s, DataRec dRec, SetRec rec, QPointF N, QPointF S, QPointF W, QPointF E);

    void setBackgroundImageFromFile(QString file_name);

    /*                            Menu: start                              */
    QMenuBar *menuBar;

    QMenu *fileMenu;
    QMenu *dataMenu;
    QMenu *palleteMenu;
    QMenu *profileMenu;
    QMenu *cutMenu;
    QMenu *helpMenu;

    // File
    QAction *newProjectAct;
    QAction *openProjectAct;
    QAction *saveProjectAct;
    QAction *saveAsProjectAct;
    QAction *exitAct;

    // Data
    QAction *addNewDataAct;
    QAction *importDataAct;
    QAction *exportDataAct;
    
    // Pallete
    QAction *addNewPalleteAct;
    QAction *genPalleteFromDataAct;
    QAction *addPalleteFromDataAct;
    QAction *importPalleteAct;
    QAction *exportPalleteAct;

    // Profile
    QAction *redrawProfileAct;
    QAction *zoomInProfileAct;
    QAction *zoomOutProfileAct;
    QAction *zoom11ProfileAct;
    QAction *zoomFitProfileAct;
    QAction *exportProfileImgAct;
    QAction *exportProfileOctaveAct;

    // Cut
    QAction *redrawCutAct;
    QAction *zoomInCutAct;
    QAction *zoomOutCutAct;
    QAction *zoom11CutAct;
    QAction *zoomFitCutAct;
    QAction *exportCutImgAct;
    
    // Help
    QAction *helpAct;
    QAction *aboutQtAct;
    QAction *aboutAct;
    
    /*                            Menu: end                                */

    QString  data_file_name;

    //QGraphicsScene *scene;
    MyGraphicsScene *scene;
    QGraphicsScene  *sceneCut;
    
    QList<DataRec> Data;
    QList<SetRec> Sets;

    /* Подложка */
    QImage  background_image;

private slots:
    void on_button_BackgroundImagePath_clicked();
    void on_button_MArea_clicked(bool checked);
    void on_checkBox_SectoringLayout_clicked(bool checked);
    void on_checkBox_TrunkLayout_clicked(bool checked);
    void on_checkBox_CrownLayout_clicked(bool checked);
    void on_radioButton_HeightSort_clicked(bool checked);
    void on_radioButton_DiameterSort_clicked(bool checked);
    void on_checkBox_HeightRestriction_clicked(bool checked);
    void on_lineEdit_HeightFrom_textEdited(QString );
    void on_lineEdit_HeightTo_textEdited(QString );
    void on_checkBox_DiameterRestriction_clicked(bool checked);
    void on_lineEdit_DiameterFrom_textEdited(QString );
    void on_lineEdit_DiameterTo_textEdited(QString );
    void on_spinBox_CutDeep_valueChanged(double );
    void on_spinBox_Transparent_valueChanged(int );
    void on_button_ZoomInCut_clicked();
    void on_button_ZoomOutCut_clicked();
    void on_button_ZoomNoCut_clicked();
    void on_button_ZoomFitCut_clicked();
    void on_button_RedrawCut_clicked();
    void on_button_MMeasure_clicked(bool checked);
    void on_button_MCut_clicked(bool checked);
    void on_table_Data_cellDoubleClicked(int row, int column);
    void on_table_Pallete_cellDoubleClicked(int row, int column);
    void on_button_ZoomFit_clicked();
    void on_button_ZoomNo_clicked();
    void on_button_ZoomOut_clicked();
    void on_button_ZoomIn_clicked();
    void on_lineEdit_ScaleFactor_textChanged(QString );
    void on_lineEdit_CrownNodesSize_textChanged(QString );
    void on_checkBox_CrownNodesShow_clicked();
    void on_button_TrunkFont_clicked();
    void on_checkBox_TrunkNumerationShow_clicked();
    void on_lineEdit_TrunkDiameterTypical_textChanged(QString );
    void on_radioButton_TrunkDiameterReal_clicked();
    void on_radioButton_TrunkDiameterTypical_clicked();
    void on_radioButton_TrunkViewCircle_clicked();
    void on_radioButton_TrunkViewPallete_clicked();
    void on_button_ColorGridMain_clicked();
    void on_button_ColorGridInter_clicked();
    void on_lineEdit_GridInterStep_textChanged(QString );
    void on_lineEdit_GridMainStep_textChanged(QString );
    void on_checkBox_GridShow_clicked();
    void on_button_MarksFont_clicked();
    void on_checkBox_MarksNumeration_clicked();
    void on_lineEdit_MarksStep_textChanged(QString );
    void on_checkBox_MarksShow_clicked();
    void on_button_ColorPlateBorder_clicked();
    void on_lineEdit_BorderWidth_textChanged(QString );
    void on_checkBox_BorderDisplay_clicked();
    void on_checkBox_ClearCrowns_clicked();
    void on_button_ColorPlate_clicked();
    void on_lineEdit_PlateShapeWidth_textChanged(QString );
    void on_lineEdit_PlateHeight_textChanged(QString );
    void on_lineEdit_PlateWidth_textChanged(QString );
    void on_button_Redraw_clicked();
    void on_backgroundImageRemove_clicked();

    void help();
    void about();
    void dataAddNew();
    void projectSaveAs();
    void projectSave();
    void projectOpen();
    void projectNew();
    void drawAll();
    void exportDataFile();
    void importPalleteFromFile();
    void exportPalleteToFile();
    void palleteAddNewFromData();
    void palleteAddNew();
    void genPalleteFromData();
    void importDataFile();
    void exportProfileImg();
    void exportCutImg();
    void exportProfileOctave();
};
#endif




