#include "mainimpl.h"

#include "version.h"

//
MainImpl::MainImpl( QWidget * parent, Qt::WindowFlags f) : QWidget(parent, f)
{
    setupUi(this);

    setWindowIcon(QIcon(":/icons/Tree_64x64.png"));

    menuBar = new QMenuBar();
    verticalLayout->insertWidget(0, menuBar);
    fillMenu();

    this->setWindowFlags(Qt::Window |
                         Qt::WindowMinMaxButtonsHint |
                         Qt::WindowCloseButtonHint);

    scene    = new MyGraphicsScene(this);
    sceneCut = new QGraphicsScene(this);
    area     = new AreaMeasure((QGraphicsScene *)scene);

    // Дополнительные действия

    // Ins в pallete table добавляет новую запись
    QShortcut *palleteAdd = new QShortcut(table_Pallete);
    palleteAdd->setKey(Qt::Key_Insert);
    palleteAdd->setContext(Qt::WidgetShortcut);
    connect(palleteAdd, SIGNAL(activated()), this, SLOT(palleteAddNew()));

    // Ins в data table добавляет новую запись
    QShortcut *dataAdd = new QShortcut(table_Data);
    dataAdd->setKey(Qt::Key_Insert);
    dataAdd->setContext(Qt::WidgetShortcut);
    connect(dataAdd, SIGNAL(activated()), this, SLOT(dataAddNew()));

    // F5 глобально перерисовывает окна отрисовки
    QShortcut *redrawAll = new QShortcut(this);
    redrawAll->setKey(Qt::Key_F5);
    //dataAdd->setContext(Qt::WidgetShortcut);
    connect(redrawAll, SIGNAL(activated()), this, SLOT(drawAll()));

    topZValue = 1000000000;

    log.setLogClass(LOG_QTEXTEDIT);
    log.setTextEdit(textEdit_Log);

    setDefaults(); // Делаем установки
    drawAll();
}
//

void MainImpl::fillMenu()
{
    // Формируем меню

    // File
    newProjectAct    = new QAction(tr("&New project"), this);
    openProjectAct   = new QAction(tr("&Open project"), this);
    saveProjectAct   = new QAction(tr("&Save project"), this);
    saveAsProjectAct = new QAction(tr("S&ave project as..."), this);
    exitAct = new QAction(tr("&Exit"), this);

    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    connect(newProjectAct, SIGNAL(triggered()), this, SLOT(projectNew()));
    connect(openProjectAct, SIGNAL(triggered()), this, SLOT(projectOpen()));
    connect(saveProjectAct, SIGNAL(triggered()), this, SLOT(projectSave()));
    connect(saveAsProjectAct, SIGNAL(triggered()), this, SLOT(projectSaveAs()));

    fileMenu = menuBar->addMenu(tr("&File"));
    fileMenu->addAction(newProjectAct);
    fileMenu->addSeparator();
    fileMenu->addAction(openProjectAct);
    fileMenu->addSeparator();
    fileMenu->addAction(saveProjectAct);
    fileMenu->addAction(saveAsProjectAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    // Data
    addNewDataAct = new QAction(tr("Add new"), this);
    importDataAct = new QAction(tr("Import &data file"), this);
    exportDataAct = new QAction(tr("Export data file"), this);

    connect(addNewDataAct, SIGNAL(triggered()), this, SLOT(dataAddNew()));
    connect(importDataAct, SIGNAL(triggered()), this, SLOT(importDataFile()));
    connect(exportDataAct, SIGNAL(triggered()), this, SLOT(exportDataFile()));

    dataMenu = menuBar->addMenu(tr("Data"));
    dataMenu->addAction(addNewDataAct);
    dataMenu->addSeparator();
    dataMenu->addAction(importDataAct);
    dataMenu->addAction(exportDataAct);

    // Pallete
    addNewPalleteAct = new QAction(tr("Add new"), this);
    importPalleteAct = new QAction(tr("Import &Pallete file"), this);
    exportPalleteAct = new QAction(tr("Export Pallete file"), this);
    genPalleteFromDataAct = new QAction(tr("Generate from data"), this);
    addPalleteFromDataAct = new QAction(tr("Add from data"), this);

    connect(genPalleteFromDataAct, SIGNAL(triggered()), this, SLOT(genPalleteFromData()));
    connect(addNewPalleteAct, SIGNAL(triggered()), this, SLOT(palleteAddNew()));
    connect(addPalleteFromDataAct, SIGNAL(triggered()), this, SLOT(palleteAddNewFromData()));
    connect(exportPalleteAct, SIGNAL(triggered()), this, SLOT(exportPalleteToFile()));
    connect(importPalleteAct, SIGNAL(triggered()), this, SLOT(importPalleteFromFile()));

    palleteMenu = menuBar->addMenu(tr("Pallete"));
    palleteMenu->addAction(addNewPalleteAct);
    palleteMenu->addSeparator();
    palleteMenu->addAction(genPalleteFromDataAct);
    palleteMenu->addAction(addPalleteFromDataAct);
    palleteMenu->addSeparator();
    palleteMenu->addAction(importPalleteAct);
    palleteMenu->addAction(exportPalleteAct);

    // Profile
    redrawProfileAct       = new QAction(tr("Redraw"), this);
    zoomInProfileAct       = new QAction(tr("Zoom In"), this);
    zoomOutProfileAct      = new QAction(tr("Zoom Out"), this);
    zoom11ProfileAct       = new QAction(tr("Zoom 1:1"), this);
    zoomFitProfileAct      = new QAction(tr("Fit to view"), this);
    exportProfileImgAct    = new QAction(tr("Export to image"), this);
    exportProfileOctaveAct = new QAction(tr("Export to Octave/Scilab"), this);

    connect(redrawProfileAct, SIGNAL(triggered()), this, SLOT(on_button_Redraw_clicked()));
    connect(zoomInProfileAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomIn_clicked()));
    connect(zoomOutProfileAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomOut_clicked()));
    connect(zoom11ProfileAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomNo_clicked()));
    connect(zoomFitProfileAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomFit_clicked()));
    connect(exportProfileImgAct, SIGNAL(triggered()), this, SLOT(exportProfileImg()));
    connect(exportProfileOctaveAct, SIGNAL(triggered()), this, SLOT(exportProfileOctave()));

    profileMenu = menuBar->addMenu(tr("Profile"));
    profileMenu->addAction(redrawProfileAct);
    profileMenu->addSeparator();
    profileMenu->addAction(zoomInProfileAct);
    profileMenu->addAction(zoomOutProfileAct);
    profileMenu->addAction(zoom11ProfileAct);
    profileMenu->addAction(zoomFitProfileAct);
    profileMenu->addSeparator();
    profileMenu->addAction(exportProfileImgAct);
    profileMenu->addAction(exportProfileOctaveAct);

    // Cut
    redrawCutAct = new QAction(tr("Redraw"), this);
    zoomInCutAct = new QAction(tr("Zoom In"), this);
    zoomOutCutAct = new QAction(tr("Zoom Out"), this);
    zoom11CutAct = new QAction(tr("Zoom 1:1"), this);
    zoomFitCutAct = new QAction(tr("Fit to view"), this);
    exportCutImgAct = new QAction(tr("Export to image"), this);

    connect(redrawCutAct, SIGNAL(triggered()), this, SLOT(on_button_RedrawCut_clicked()));
    connect(zoomInCutAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomInCut_clicked()));
    connect(zoomOutCutAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomOutCut_clicked()));
    connect(zoom11CutAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomNoCut_clicked()));
    connect(zoomFitCutAct, SIGNAL(triggered()), this, SLOT(on_button_ZoomFitCut_clicked()));
    connect(exportCutImgAct, SIGNAL(triggered()), this, SLOT(exportCutImg()));

    cutMenu = menuBar->addMenu(tr("Cut"));
    cutMenu->addAction(redrawCutAct);
    cutMenu->addSeparator();
    cutMenu->addAction(zoomInCutAct);
    cutMenu->addAction(zoomOutCutAct);
    cutMenu->addAction(zoom11CutAct);
    cutMenu->addAction(zoomFitCutAct);
    cutMenu->addSeparator();
    cutMenu->addAction(exportCutImgAct);

    // Help
    helpAct = new QAction(tr("Help"), this);
    aboutQtAct = new QAction(tr("About Qt"), this);
    aboutAct = new QAction(tr("About Crowns"), this);

    connect(helpAct,    SIGNAL(triggered()), this, SLOT(help()));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(aboutAct,   SIGNAL(triggered()), this, SLOT(about()));

    helpMenu = menuBar->addMenu(tr("&Help"));
    helpMenu->addAction(helpAct);
    helpMenu->addSeparator();
    helpMenu->addAction(aboutQtAct);
    helpMenu->addAction(aboutAct);

}
//

void MainImpl::importDataFile()
{
    // открытие файла данных
    QString filter;
    TxtDataFormat format = FORMAT_V3;

    QString filter_delim = ";;";
    QString filter_txt1   = "Data in TXT version 1 (*.txt)";
    QString filter_txt2   = "Data in TXT version 2 / with Height (*.txt)";
    QString filter_txt3   = "Data in TXT version 3 / with Age (*.txt)";
    QString filter_crown  = "Data in native format (*.crown)";
    QString filter_all    = "All files (*)";

    QFileDialog data_open_dlg(this, tr("Open data file"), "",
                    filter_txt3  + filter_delim +
                    filter_txt2  + filter_delim +
                    filter_txt1  + filter_delim +
                    filter_crown + filter_delim +
                    filter_all);

    if (!Data.isEmpty())
    {
        QMessageBox mb(QMessageBox::Warning,
                       tr("Data file import"),
                       tr("Data already exists. This action will override it. Continue?"),
                       QMessageBox::Yes|QMessageBox::No,
                       this);

        if (mb.exec() == QMessageBox::No)
        {
            return;
        }
    }

    if (!data_open_dlg.exec())
    {
        return;
    }

    if (data_open_dlg.selectedFiles().size() < 1)
    {
        return;
    }

    data_file_name = data_open_dlg.selectedFiles().at(0);
    filter         = data_open_dlg.selectedNameFilter();

    if (filter == filter_crown)
    {
        importDataFromIniFile(data_file_name);
    }
    else
    {
        // По умолчанию формат V3, или самый старший
        if (filter == filter_txt1)
            format = FORMAT_V1;
        else if (filter == filter_txt2)
            format = FORMAT_V2;

        importDataFromTxtFile(data_file_name, format);
    }

    projSaved = false;
    sortData();
    drawPlot();
}


void MainImpl::importDataFromTxtFile(QString fileName, TxtDataFormat format)
{
    // Загружаем структуру данных
    float Nx, Sx, Wx, Ex;
    float Ny, Sy, Wy, Ey;
    float X = 0.0, Y = 0.0;
    float D = 0.0;
    float H = 0.0;
    float age = 0.0;
    int spCode;
    int TreeNum;

    QFile data_file;

    if (data_file.exists(fileName))
    {
        data_file.setFileName(fileName);
        if (data_file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&data_file);

            in.readLine(); // Пропустить первую строчку
            in.readLine(); // Пропустить вторую строку, читаем пока не закончатся данные

            Data.clear();
            TreeNums.clear();
            clearTable(table_Data);

            while (!in.atEnd())
            {
                DataRec item;

                switch (format)
                {
                    case FORMAT_V1:
                    {
                        in >> TreeNum >> X >> Y >> Nx >> Ny >> Sx >> Sy >> Wx >> Wy
                                >> Ex >> Ey >> D >> spCode;
                        break;
                    }
                    case FORMAT_V2:
                    {
                        in >> TreeNum >> X >> Y >> Nx >> Ny >> Sx >> Sy >> Wx >> Wy
                                >> Ex >> Ey >> D >> H >> spCode;
                        break;
                    }

                    case FORMAT_V3:
                    default:
                    {
                        in >> TreeNum >> X >> Y >> Nx >> Ny >> Sx >> Sy >> Wx >> Wy
                                >> Ex >> Ey >> D >> H >> age >> spCode;
                        break;
                    }
                }

                if (TreeNum == 0 || spCode == 0)
                {
                    continue;
                }

                item.TreeNum = TreeNum;
                item.X = X;
                item.Y = Y;
                item.Nx = Nx;
                item.Ny = Ny;
                item.Sx = Sx;
                item.Sy = Sy;
                item.Wx = Wx;
                item.Wy = Wy;
                item.Ex = Ex;
                item.Ey = Ey;
                item.D = D;
                item.H = H;
                item.age = age;
                item.spCode = spCode;

                Data.append(item);
                addSetDataItem(-1, item);
                TreeNums.append(TreeNum);
            }
        }
        data_file.close();
    }
}


void MainImpl::drawPlot()
{
    float Nx, Sx, Wx, Ex;
    float Ny, Sy, Wy, Ey;
    float X, Y;
    float D, H;

    int spCode;
    int TreeNum;

    float center;

    // Размеры плантации в метрах
    int plateW = getPlateWidth();
    int plateH = getPlateHeight();

    // Отступ по краям в метрах
    int shiftP = getPlateShapeWidth();

    // Размер отсечек на границе профиля
    int crS = getCrownNodesSize();

    QFont  oldFont,  font;
    QPen   oldPen,   pen;
    QBrush oldBrush, brush;

    // Фактор масштаба
    int scale = getScaleFactor();

    // Размеры зоны отображения
    int gvW = (plateW + 2*shiftP) * scale;
    int gvH = (plateH + 2*shiftP) * scale;

    scene->clear();
    scene->setSceneRect(0, 0, gvW, gvH);
    scene->setBackgroundBrush(getProbePlateColor()); // Заполним картинку цветом заливки пробной площади

    uint       layout       = getLayouts();
    uint       restrictions = getRestrictions();
    RestValues restVal      = getRestValues();

    // Отрисовка разметки пробной площади
    if (layout & LAYOUT_SECTORING)
    {
        drawPlotBase(scene, shiftP, scale, plateW, plateH, 1);
    }

    zValue = 10;
    // Основной цикл прорисовки деревьев
    for (int i=0; i < Data.size(); i++)
    {
        DataRec dat = Data.at(i);

        MathPrepare(&dat);

        TreeNum = dat.TreeNum;
        X = dat.X;
        Y = dat.Y;
        Nx = dat.Nx;
        Ny = dat.Ny;
        Sx = dat.Sx;
        Sy = dat.Sy;
        Wx = dat.Wx;
        Wy = dat.Wy;
        Ex = dat.Ex;
        Ey = dat.Ey;
        spCode = dat.spCode;
        H = dat.H;
        D = dat.D;

        // Проверим ограничения, если они выполняются, перейти к следующей итерации
        if ((restrictions & REST_HEIGHT) &&
                ((H < restVal.height_min) ||
                 (H > restVal.height_max))) /* По высоте */
        {
            continue;
        }

        if ((restrictions & REST_DIAMETER) &&
                ((D < restVal.diameter_min) ||
                 (D > restVal.diameter_max))) /* По диаметру */
        {
            continue;
        }

        if ((restrictions & REST_AGE) &&
                ((dat.age < restVal.age_min) ||
                 (dat.age > restVal.age_max)))
        {
            continue;
        }

        SetRec rec = getSetRec(spCode);
        if (rec.draw == 0)
        {
            continue;
        }

        X  = (X + shiftP) * scale;
        Y  = (Y + shiftP) * scale;
        Nx = X + Nx * scale;
        Ny = Y - Ny * scale;
        Sx = X + Sx * scale;
        Sy = Y + Sy * scale;
        Wx = X - Wx * scale;
        Wy = Y + Wy * scale;
        Ex = X + Ex * scale;
        Ey = Y + Ey * scale;

        // --------------- //
        // Прорисовка крон //
        // --------------- //
        if (layout & LAYOUT_CROWNS)
        {
            //QPointF P1, P2;

            printf("POST: N(%f, %f), S(%f, %f), W(%f, %f), E(%f, %f)\n",
                   Nx, Ny,
                   Sx, Sy,
                   Wx, Wy,
                   Ex, Ey);

            qDebug() << "TreeNum: " << TreeNum;

            drawCrown(scene, Data.at(i), rec,
                      QPointF(Nx, Ny),
                      QPointF(Sx, Sy),
                      QPointF(Wx, Wy),
                      QPointF(Ex, Ey));

/*
 //  Не удалять кусок кода
 // @TODO оформить в виде отдельной функции, предоставлять выбор какой механизм отрисовки использовать
            P1.setX(Nx - (Ex - Nx));
            P1.setY(Ny);
            P2.setX(Nx + (Ex - Nx));
            P2.setY(Ey + (Ey - Ny));
            drawCrownPart(scene, Data.at(i), rec, P1, P2, Ex, Ey, Nx, Ny);

            P1.setX(Nx - (Nx - Wx));
            P1.setY(Ny);
            P2.setX(Nx + (Nx - Wx));
            P2.setY(Wy + (Wy - Ny));
            drawCrownPart(scene, Data.at(i), rec, P1, P2, Nx, Ny, Wx, Wy);

            P1.setX(Wx);
            P1.setY(Wy - (Sy - Wy));
            P2.setX(Sx + (Sx - Wx));
            P2.setY(Sy);
            drawCrownPart(scene, Data.at(i), rec, P1, P2, Wx, Wy, Sx, Sy);

            P1.setX(Sx - (Ex - Sx));
            P1.setY(Ey - (Sy - Ey));
            P2.setX(Ex);
            P2.setY(Sy);
            drawCrownPart(scene, Data.at(i), rec, P1, P2, Sx, Sy, Ex, Ey);
*/

            // Отсечки на границе профилей (точки исходных данных)
            if (getIsCrownNodesShow())
            {
                scene->addLine(Nx - crS, Ny - crS, Nx + crS, Ny + crS);
                scene->addLine(Nx - crS, Ny + crS, Nx + crS, Ny - crS);

                scene->addLine(Sx - crS, Sy - crS, Sx + crS, Sy + crS);
                scene->addLine(Sx - crS, Sy + crS, Sx + crS, Sy - crS);

                scene->addLine(Wx - crS, Wy - crS, Wx + crS, Wy + crS);
                scene->addLine(Wx - crS, Wy + crS, Wx + crS, Wy - crS);

                scene->addLine(Ex - crS, Ey - crS, Ex + crS, Ey + crS);
                scene->addLine(Ex - crS, Ey + crS, Ex + crS, Ey - crS);
            }
        }

        // ------------------ //
        // Прорисовка стволов //
        // ------------------ //
        if (layout & LAYOUT_TRUNKS)
        {
            switch (getTrunkView())
            {
                case TRUNK_VIEW_CIRCLE: /* Рисовать ствол кружком */
                {
                    center = 0.0;
                    switch (getTrunkDiameter())
                    {
                        case TRUNK_DIAMETER_REAL:
                        {
                            center = (qreal)D / 100.0 / 2.0 * scale; // TODO: в каких единицах задан радиус?
                            break;
                        }

                        case TRUNK_DIAMETER_TYPO:
                        {
                            center = (float)getTrunkDiameterTypoSize();
                            break;
                        }
                    }

                    // TODO: установить цвета отрисовки по палитре и коду
                    pen.setWidth(1);
                    pen.setColor(rec.trunkColor);

                    if (rec.code != -1)
                    {
                        brush.setColor(rec.trunkColor);
                        brush.setStyle(Qt::SolidPattern);
                    }
                    QGraphicsEllipseItem *el;
                    el = scene->addEllipse(QRectF(X-center, Y-center, center*2, center*2), pen, brush);
                    el->setZValue(topZValue);
                    el->setToolTip(genTreeToolTip(Data.at(i), rec));
                    break;
                }

                case TRUNK_VIEW_PALLETE: /* Рисовать ствол по данным палитры */
                {
                    // TODO: Реализовать отрисовку из палитры
                    break;
                }
            }

            // Подписи номера стволов
            if (getIsTrunkNumerationShow())
            {
                font = getTrunkFont();
                QGraphicsTextItem *txt;

                txt = scene->addText(QString("%1").arg(TreeNum), font);
                txt->setPos(X + center + 2.0, Y);
                txt->setZValue(topZValue);
            }
        }

        zValue++;
    }

    // Отрисовка разметки пробной площади
    if (layout & LAYOUT_SECTORING)
    {
        drawPlotBase(scene, shiftP, scale, plateW, plateH, 2);
    }

    graphicsView->setScene(scene);

}

// Вспомогательная функция для расчета двух вспомогательных точек для строительства дуги между двумя
// при помощи кривой безье, вынесено отдельно т.к. используется в двух местах, возможно будет
// вынесена в common.cpp/common.h
static inline void calcControlPoints(QPointF p0, QPointF p1, QPointF &ctrl0, QPointF &ctrl1)
{
    double x1 = p0.x();
    double y1 = p0.y();
    double x2 = p1.x();
    double y2 = p1.y();
    double xc1; // первая вспомогательная точка
    double yc1;
    double xc2; // вторая вспомогательная точка
    double yc2;

    double dx;
    double dy;

    dx = (x2 - x1) / 2.0;
    dy = (y2 - y1) / 2.0;

    if (SIGN(dx) == SIGN(dy))
    {
        xc1 = x1;
        yc1 = y1 + dy;

        xc2 = x1 + dx;
        yc2 = y2;
    }
    else if (dy < 0.0)
    {
        xc1 = x1 + fabs(dx);
        yc1 = y1;

        xc2 = x2;
        yc2 = y2 + fabs(dy);
    }
    else if (dx < 0.0)
    {
        xc1 = x1 + dx;
        yc1 = y1;

        xc2 = x2;
        yc2 = y1 + fabs(dy);
    }

    ctrl0.setX(xc1);
    ctrl0.setY(yc1);
    ctrl1.setX(xc2);
    ctrl1.setY(yc2);
}

void MainImpl::drawCrown(QGraphicsScene *s, DataRec dRec, SetRec rec, QPointF N, QPointF S, QPointF W, QPointF E)
{
    QList<QPointF> crown_xy;
    QPainterPath   crown_path;

    // Заполняем список, точки против часовой стрелки начиная с Западной (хотя можно с любой)
    // если бы Y росло вверх, обход делали бы по часовой стрелке
    crown_xy << W << S << E << N;

    crown_path.moveTo(W);

    for (int i = 0; i < crown_xy.count(); i++)
    {
        QPointF p0;
        QPointF p1;
        QPointF c0; // Первая контрольная точка
        QPointF c1; // Вторая контрольная точка

        p0 = crown_xy[i];
        if (i == crown_xy.count() - 1) // в общем случае 4
        {
            p1 = crown_xy[0];
        }
        else
        {
            p1 = crown_xy[i + 1];
        }

        calcControlPoints(p0, p1, c0, c1);
        crown_path.cubicTo(c0, c1, p1);
    }

    QPen               pen;
    QBrush             brush;
    QGraphicsPathItem *path;

    //
    if (rec.isFill)
    {
        QColor bColor = rec.projFillColor;
        bColor.setAlpha(getTransVal());
        brush.setColor(bColor);
        brush.setStyle((Qt::BrushStyle)rec.projFillStyle);
    }

    pen.setWidth(rec.projLineWidth);
    pen.setColor(rec.projLineColor);
    //pen.setColor(Qt::red); // DEBUG

    path = s->addPath(crown_path, pen, brush);
    path->setZValue(zValue);
    path->setToolTip(genTreeToolTip(dRec, rec));
}

void MainImpl::drawCrownPart(QGraphicsScene *s, DataRec dRec, SetRec rec, QPointF P1, QPointF P2, qreal X1, qreal Y1, qreal X2, qreal Y2)
{
    // Рисует часть кроны по заданным параметрам
    QRectF  rect;
    float Cx, Cy;
    float x1, x2, y1, y2; // вспомогательные переменные для определения углов
    float startA = 0.0;
    float endA   = 0.0;
    float spanA  = 0.0;
    float cosSA  = 0.0; // косинус начального угла
    float cosEA  = 0.0; // косинус конечного угла

    // Центр элипса
    Cx = P1.x() + (P2.x() - P1.x()) / 2.0;
    Cy = P1.y() + (P2.y() - P1.y()) / 2.0;

    x1 = Cx - P1.x(); // координата x первого вектора
    y1 = Cy - Cy;     // координата y первого вектора, 0, но вычисление оставил

    x2 = X1 - Cx;
    y2 = Y1 - Cy;
    cosSA = (x1*x2 + y1*y2) / sqrt((x1*x1 + y1*y1) * (x2*x2 + y2*y2));
    startA = acos(cosSA) * (180.0/M_PI);

    x2 = X2 - Cx;
    y2 = Y2 - Cy;
    cosEA = (x1*x2 + y1*y2) / sqrt((x1*x1 + y1*y1) * (x2*x2 + y2*y2));
    endA = acos(cosEA) * (180.0/M_PI);

    qDebug() << "1. startA = " << startA << " endA = " << endA << " spanA = " << spanA;

    if (Y1 > Cy)
    {
        startA = 360.0 - startA;
    }

    if (Y2 > Cy)
    {
        endA = 360.0 - endA;
    }

    if (endA < startA)
    {
        spanA = 360 - startA + endA;
    }
    else
    {
        spanA = endA - startA;
    }

    qDebug() << "2. startA = " << startA << " endA = " << endA << " spanA = " << spanA;

    /*
    printf("%f / %f / %f\n", startA/16.0, endA/16.0, spanA/16.0);
    */

    rect.setTopLeft(P1);
    rect.setBottomRight(P2);

    QPen   pen, oldPen;
    QBrush brush, oldBrush;

    QGraphicsPathItem *path;

    if (rec.isFill)
    {
        pen.setWidth(1);
        pen.setColor(Qt::transparent);

        //brush.setColor(Qt::green);
        QColor bColor = rec.projFillColor;
        bColor.setAlpha(getTransVal());
        brush.setColor(bColor);
        //brush.setStyle(Qt::SolidPattern);
        brush.setStyle((Qt::BrushStyle)rec.projFillStyle);

        // Задание цвета и опциональная закраска
        path = scene->addPath(piePath(rect, startA, spanA), pen, brush);
        path->setZValue(zValue);
        path->setToolTip(tr("%1. %2").arg(dRec.TreeNum).arg(rec.spName));
    }

    pen.setWidth(rec.projLineWidth);
    pen.setColor(rec.projLineColor);

    path = s->addPath(arcPath(rect, startA, spanA), pen);
    path->setZValue(zValue);
    path->setToolTip(genTreeToolTip(dRec, rec));
}


void MainImpl::drawPlotBase(QGraphicsScene *s, int shiftP, int scale, int w, int h, int stage)
{
    // TODO
    // Базовая отрисовка
    QPen   pen, oldPen;
    QBrush brush, oldBrush;
    QFont  font, oldFont;

    if (stage == 2)
    {
        // Очистка пустого поля вокруг площади
        if (getIsClearCrowns())
        {
            pen.setColor(Qt::transparent);

            QColor bColor = getProbePlateColor();
            brush.setColor(bColor);
            brush.setStyle(Qt::SolidPattern);

            s->addRect(1, 1, (w + 2*shiftP) * scale, shiftP * scale, pen, brush);
            s->addRect(1, 1, shiftP * scale, (h + 2 * shiftP) * scale, pen, brush);

            s->addRect((w + shiftP) * scale, 1, shiftP * scale, (h + 2 * shiftP) * scale, pen, brush);
            s->addRect(1, (h + shiftP) * scale, (w + 2*shiftP) * scale, shiftP * scale, pen, brush);
        }
    }
    else
    {
        if (!background_image.isNull())
        {
            //
            int pixel_width  = background_image.width();
            int pixel_height = background_image.height();

            int meter_width  = line_BackgroundImageSizeX->text().toInt();
            int meter_height = line_BackgroundImageSizeY->text().toInt();

            int plate_meter_width =  getPlateWidth();
            int plate_meter_height = getPlateHeight();

            int scale = getScaleFactor();

            if (meter_width < 0 || meter_height < 0)
            {
                meter_width  = plate_meter_width;
                meter_height = plate_meter_height;
            }

            int new_pixel_width  = meter_width  * scale;
            int new_pixel_height = meter_height * scale;

            QImage background_tmp = background_image.scaled(new_pixel_width,
                                                            new_pixel_height,
                                                            Qt::KeepAspectRatio);

            //
            QGraphicsPixmapItem *pixmap_item;
            pixmap_item = scene->addPixmap(QPixmap::fromImage(background_tmp));
            pixmap_item->setPos(shiftP * scale, shiftP * scale);
        }
    }

    // Отображение границ площадей
    if (getIsPlateBorderDisplay())
    {
        pen.setWidth(getBorderWidth());
        pen.setColor(getColorPlateBorder());

        scene->addRect(1, 1, (w + 2*shiftP) * scale, (h + 2 * shiftP) * scale, pen, brush);
        scene->addRect(shiftP*scale, shiftP*scale, w*scale, h*scale, pen, brush);
    }

    // Отображение отсечек на осях
    if (getIsMarksShow())
    {
        drawBorderMarks(s, w, h, shiftP, 1);
    }

    // Отображаем сетку
    if (getIsGridShow())
    {
        drawGrid(s, w, h, shiftP);
    }

}

void MainImpl::exportProfileImg()
{
    // Экспортируем картинку с профилями
    int w = (getPlateWidth() + 2*getPlateShapeWidth()) * getScaleFactor() * 2;
    int h = (getPlateHeight() + 2*getPlateShapeWidth()) * getScaleFactor() * 2;

    exportToImg(graphicsView->scene(), w, h);
}

uint MainImpl::getLayouts()
{
    // Возвращает маску слоёв
    uint ret = LAYOUT_NONE;

    if (checkBox_SectoringLayout->isChecked())
    {
        ret |= LAYOUT_SECTORING;
    }

    if (checkBox_TrunkLayout->isChecked())
    {
        ret |= LAYOUT_TRUNKS;
    }

    if (checkBox_CrownLayout->isChecked())
    {
        ret |= LAYOUT_CROWNS;
    }

    return ret;
}

uint MainImpl::getSortType()
{
    // Возвращает тип сортировки
    uint ret = 0;

    if (radioButton_HeightSort->isChecked())
    {
        ret = SORT_HEIGHT;
    }
    else
    {
        ret = SORT_DIAMETER;
    }

    return ret;
}

uint MainImpl::getRestrictions()
{
    // возвращает виды ограничений на отрисовку
    uint ret = REST_NONE;

    if (checkBox_HeightRestriction->isChecked())
    {
        ret |= REST_HEIGHT;
    }

    if (checkBox_DiameterRestriction->isChecked())
    {
        ret |= REST_DIAMETER;
    }

    if (checkBox_AgeRestriction->isChecked())
    {
        ret |= REST_AGE;
    }

    return ret;
}

void MainImpl::on_button_Redraw_clicked()
{
    // Перерисовать изображение
    if (Data.size() > 0)
    {
        sortData();
        drawPlot();
    }
}

void MainImpl::sortData()
{
    // Сортировка данных
    // TODO: не нужна???

    uint sortType = getSortType();
    if (sortType == SORT_HEIGHT)
    {
        std::sort(Data.begin(), Data.end(), sortDataCompareHeight);
    }
    else if (sortType == SORT_DIAMETER)
    {
        std::sort(Data.begin(), Data.end(), sortDataCompareDiameter);
    }

    TreeNums.clear();
    for (int i = 0; i< Data.size(); i++)
    {
        TreeNums.append(Data.at(i).TreeNum);
    }
}


RestValues MainImpl::getRestValues()
{
    // Возвращает значение ограничений на отрисовку
    RestValues ret;

    ret.height_min   = lineEdit_HeightFrom->text().toUInt();
    ret.height_max   = lineEdit_HeightTo->text().toUInt();
    ret.diameter_min = lineEdit_DiameterFrom->text().toUInt();
    ret.diameter_max = lineEdit_DiameterTo->text().toUInt();
    ret.age_min      = lineEdit_AgeFrom->text().toUInt();
    ret.age_max      = lineEdit_AgeTo->text().toUInt();

    return ret;
}

void MainImpl::on_lineEdit_PlateWidth_textChanged(QString text)
{
    // Устанавливает новую ширину пробной площади
    plateWidth = text.toInt();
    projSaved = false;
}

void MainImpl::on_lineEdit_PlateHeight_textChanged(QString text)
{
    // Устанавливает новую высоту пробной площади
    plateHeight = text.toInt();
    projSaved = false;
}

void MainImpl::on_lineEdit_PlateShapeWidth_textChanged(QString text)
{
    // Устанавливает ширину пустого поля вокруг пробной площади
    plateShapeWidth = text.toInt();
    projSaved = false;
}

void MainImpl::on_button_ColorPlate_clicked()
{
    // Устанавливаем цвет пробной площади
    QColorDialog color_dlg(getProbePlateColor(), this);

    if (!color_dlg.exec())
    {
        return;
    }

    setProbePlateColor(color_dlg.selectedColor());
    projSaved = false;
}

void MainImpl::on_checkBox_ClearCrowns_clicked()
{
    // Устанавливает параметр очистки крон за пределами пробной области
    isClearCrowns = checkBox_ClearCrowns->isChecked();
    projSaved = false;
}

void MainImpl::on_checkBox_BorderDisplay_clicked()
{
    // Устанавливает параметр отображение границы пробной площади
    isPlateBorderDisplay = checkBox_BorderDisplay->isChecked();
    projSaved = false;
}

void MainImpl::on_lineEdit_BorderWidth_textChanged(QString text)
{
    // Устанавливает толщину линиц границ площади
    borderWidth = text.toInt();
    projSaved = false;
}

void MainImpl::on_button_ColorPlateBorder_clicked()
{
    // Устанавливаем цвет границы пробной площади
    QColorDialog color_dlg(getColorPlateBorder(), this);

    if (!color_dlg.exec())
    {
        return;
    }

    setColorPlateBorder(color_dlg.selectedColor());
    projSaved = false;
}

void MainImpl::on_checkBox_MarksShow_clicked()
{
    // Показывать отметки на полях пробной площади
    isMarksShow = checkBox_MarksShow->isChecked();
    projSaved = false;
}

void MainImpl::on_lineEdit_MarksStep_textChanged(QString text)
{
    // Устанавливает шаг отметок на осях
    marksStep = text.toInt();
    projSaved = false;
}

void MainImpl::on_checkBox_MarksNumeration_clicked()
{
    // Показывать номера на отметках
    isMarksNumeration = checkBox_MarksNumeration->isChecked();
    projSaved = false;
}

void MainImpl::on_button_MarksFont_clicked()
{
    // Устанавливает шрифт для отрисовки цифровых меток на осях
    QFontDialog font_dlg(getMarksFont(), this);

    if (!font_dlg.exec())
    {
        return;
    }

    setMarksFont(font_dlg.selectedFont());
    projSaved = false;
}

void MainImpl::on_checkBox_GridShow_clicked()
{
    // Устанавливает отображение сетки
    isGridShow = checkBox_GridShow->isChecked();
    projSaved = false;
}

void MainImpl::on_lineEdit_GridMainStep_textChanged(QString text)
{
    // Устанавливает шаг основной сетки
    gridMainStep = text.toInt();
    projSaved = false;
}

void MainImpl::on_lineEdit_GridInterStep_textChanged(QString text)
{
    // Устанавливает шаг промежуточной сетки
    gridInterStep = text.toInt();
    projSaved = false;
}

void MainImpl::on_button_ColorGridMain_clicked()
{
    // Устанавливает шрифт основной сетки
    QColorDialog color_dlg(getColorGridMain(), this);

    if (!color_dlg.exec())
    {
        return;
    }

    setColorGridMain(color_dlg.selectedColor());
    projSaved = false;
}

void MainImpl::on_button_ColorGridInter_clicked()
{
    // Устанавливает шрифт дополнительной сетки
    QColorDialog color_dlg(getColorGridInter(), this);

    if (!color_dlg.exec())
    {
        return;
    }

    setColorGridInter(color_dlg.selectedColor());
    projSaved = false;
}

void MainImpl::on_radioButton_TrunkViewCircle_clicked()
{
    // Устанавливает вид отображения дерева в Circle
    trunkView = TRUNK_VIEW_CIRCLE;
    projSaved = false;
}

void MainImpl::on_radioButton_TrunkViewPallete_clicked()
{
    // Устанавливает вид отображения дерева в Pallete (задано палитрой)
    trunkView = TRUNK_VIEW_PALLETE;
    projSaved = false;
}

void MainImpl::on_radioButton_TrunkDiameterReal_clicked()
{
    // Устанавливает тип диаметра ствола
    trunkDiameter = TRUNK_DIAMETER_REAL;
    projSaved = false;
}

void MainImpl::on_radioButton_TrunkDiameterTypical_clicked()
{
    // Устанавливает тип диаметра ствола
    trunkDiameter = TRUNK_DIAMETER_TYPO;
    projSaved = false;
}

void MainImpl::on_lineEdit_TrunkDiameterTypical_textChanged(QString text)
{
    // Устанавливает условный диаметр ствола
    trunkDiameterTypoSize = text.toInt();
    projSaved = false;
}

void MainImpl::on_checkBox_TrunkNumerationShow_clicked()
{
    // Устанавливает отображение номеро деревьев
    isTrunkNumerationShow = checkBox_TrunkNumerationShow->isChecked();
    projSaved = false;
}

void MainImpl::on_button_TrunkFont_clicked()
{
    // Выбрать шрифт для номеров деревьев
    QFontDialog font_dlg(getTrunkFont(), this);

    if (!font_dlg.exec())
    {
        return;
    }

    setTrunkFont(font_dlg.selectedFont());
    projSaved = false;
}

void MainImpl::on_checkBox_CrownNodesShow_clicked()
{
    // Показывать отметки на границе профиля
    isCrownNodesShow = checkBox_CrownNodesShow->isChecked();
    projSaved = false;
}

void MainImpl::on_lineEdit_CrownNodesSize_textChanged(QString text)
{
    // Устанавливает размер отметок на границе профиля
    crownNodesSize = text.toInt();
    projSaved = false;
}

void MainImpl::on_lineEdit_ScaleFactor_textChanged(QString text)
{
    // Устанавливает масштаб отрисовки - сколько точек в одном метре
    scaleFactor = text.toInt();
    projSaved = false;
}

void MainImpl::on_button_ZoomIn_clicked()
{
    // Zoom in
    if (graphicsView->transform().m11() < pow(1.1, 10))
    {
        graphicsView->scale(1.1, 1.1);
    }

}

void MainImpl::on_button_ZoomOut_clicked()
{
    // Zoom out
    if (graphicsView->transform().m11() > pow(0.9, 10))
    {
        graphicsView->scale(0.9, 0.9);
    }
}

void MainImpl::on_button_ZoomNo_clicked()
{
    // Zoom 1:1
    graphicsView->resetTransform();
}

void MainImpl::on_button_ZoomFit_clicked()
{
    // Zoom fit
    graphicsView->fitInView(0, 0, scene->width(), scene->height(), Qt::KeepAspectRatio);
}

void MainImpl::genPalleteFromData()
{
    // Генерирует палитру с умолчательными значениями на основе пород деревьев в данных
    QList<int> Codes;
    int i=0;

    if (!Sets.isEmpty())
    {
        QMessageBox mb(QMessageBox::Warning,
                       QString("Crowns Pallete generate"),
                       QString("Pallete already exists. This action will override it. Continue?"),
                       QMessageBox::Yes|QMessageBox::No,
                       this);

        if (mb.exec() == QMessageBox::No)
        {
            return;
        }
    }

    clearTable(table_Pallete);

    Sets.clear();
    Codes.clear();
    // пробежимся по данным и сформируем список кодов пород
    for (i = 0; i < Data.size(); i++)
    {
        int spCode = Data.at(i).spCode;

        if (Codes.indexOf(spCode) == -1)
        {
            Codes.append(spCode);
        }
    }

    // Собственно по кодам пород и побежали...
    for (i = 0; i < Codes.size(); i++)
    {
        SetRec rec;
        setRecFillDefault(&rec);
        rec.code = Codes.at(i);
        Sets.append(rec);
    }

    // Отсортируем по возрастанию типа
    std::sort(Sets.begin(), Sets.end(), sortSetsByType);

    for (i = 0; i < Sets.size(); i++)
    {
        addSetPalleteItem(-1, Sets.at(i));
    }
    projSaved = false;
}

void MainImpl::addSetPalleteItem(int index, SetRec set)
{
    // Добавляем в таблицу с палитрой значение
    int w = 80, h = 24;

    QLabel *code;
    QLabel *type;
    QLabel *name;
    QLabel *show;
    QLabel *trunk;
    QLabel *crown;
    QLabel *fill;

    QPixmap trunk_pix(w, h);
    QPixmap crown_pix(w, h);
    QPixmap fill_pix(w, h);
    QPixmap tmp_pix;

    QPainter p;
    QPen   oldPen, pen;
    QBrush oldBrush, brush;

    int oldIndex = index;

    oldPen = pen = p.pen();
    oldBrush = brush = p.brush();

    if (oldIndex == -1)
    {
        // Добавляем
        table_Pallete->setRowCount(table_Pallete->rowCount() + 1);
        index = table_Pallete->rowCount() - 1;

        code  = new QLabel();
        type  = new QLabel();
        name  = new QLabel();
        show  = new QLabel();
        trunk = new QLabel();
        crown = new QLabel();
        fill  = new QLabel();

    }
    else
    {
        // Изменяем
        code  = (QLabel *)table_Pallete->cellWidget(index, 0);
        type  = (QLabel *)table_Pallete->cellWidget(index, 1);
        name  = (QLabel *)table_Pallete->cellWidget(index, 2);
        show  = (QLabel *)table_Pallete->cellWidget(index, 3);
        trunk = (QLabel *)table_Pallete->cellWidget(index, 4);
        crown = (QLabel *)table_Pallete->cellWidget(index, 5);
        fill  = (QLabel *)table_Pallete->cellWidget(index, 6);
    }

    code->setText(QString("%1").arg(set.code));
    type->setText(QString("%1").arg(set.spType));
    name->setText(set.spName);
    show->setText(set.draw==true?tr("Yes"):tr("No"));

    trunk_pix.fill(Qt::white);
    p.begin(&trunk_pix);
    pen.setColor(set.trunkColor);
    brush.setColor(set.trunkColor);
    brush.setStyle(Qt::SolidPattern);
    p.setPen(pen);
    p.setBrush(brush);
    p.drawEllipse(w/2 - MIN(w, h)/2, h/2 - MIN(w, h)/2, MIN(w, h), MIN(w, h));
    p.end();
    trunk->setPixmap(trunk_pix);

    crown_pix.fill(Qt::white);
    p.begin(&crown_pix);
    pen.setColor(set.projLineColor);
    pen.setWidth(set.projLineWidth);
    pen.setStyle((Qt::PenStyle)set.projLineStyle);
    //brush.setColor(set.trunkColor);
    //brush.setStyle(Qt::SolidPattern);
    p.setPen(pen);
    p.setBrush(oldBrush);
    p.drawLine(3, h/2, w-3, h/2);
    p.end();
    crown->setPixmap(crown_pix);

    if (set.isFill)
    {
        //fill_pix.fill(set.projFillColor);
        fill_pix.fill(Qt::white);
        p.begin(&fill_pix);
        pen.setColor(Qt::black);
        pen.setWidth(1);
        pen.setStyle(Qt::SolidLine);

        brush.setColor(set.projFillColor);
        brush.setStyle((Qt::BrushStyle)set.projFillStyle);

        p.setPen(pen);
        p.setBrush(brush);
        p.drawRect(0,0,w-1,h-1);

        p.end();
    }
    else
    {
        fill_pix.fill(Qt::white);
        p.begin(&fill_pix);
        pen.setColor(Qt::black);
        pen.setWidth(1);
        pen.setStyle(Qt::SolidLine);
        //brush.setColor(set.trunkColor);
        //brush.setStyle(Qt::SolidPattern);
        p.setPen(pen);
        p.setBrush(oldBrush);
        p.drawRect(0,0,w-1,h-1);
        pen.setColor(Qt::red);
        p.setPen(pen);
        p.drawLine(1,1,w-3,h-3);
        p.end();
    }
    fill->setPixmap(fill_pix);

    if (oldIndex == -1)
    {
        table_Pallete->setCellWidget(index, 0, code);
        table_Pallete->setCellWidget(index, 1, type);
        table_Pallete->setCellWidget(index, 2, name);
        table_Pallete->setCellWidget(index, 3, show);
        table_Pallete->setCellWidget(index, 4, trunk);
        table_Pallete->setCellWidget(index, 5, crown);
        table_Pallete->setCellWidget(index, 6, fill);
    }
}


bool MainImpl::event(QEvent *ev)
{
    // Переопределённая функция обработки событий. ловим событие с координатами
    return QWidget::event(ev);
}


void MainImpl::on_table_Pallete_cellDoubleClicked(int row, int)
{
    // Двойной клик на строке
    PalleteEditImpl dlg(this, Qt::Dialog);

    SetRec rec = Sets.at(row);

    dlg.setPalleteItem(rec);

    if (dlg.exec())
    {
        rec = dlg.getPalleteItem();
        Sets.replace(row, rec);
        addSetPalleteItem(row, rec);
        projSaved = false;
    }
}

SetRec MainImpl::getSetRec(int spCode)
{
    // Возвращает запись с параметрами породы по её коду.
    // если такого кода найдено не было, в записи возвратит умолчательные значениия
    // и номер code -1 - проверять и отрисовывать.

    SetRec rec;

    setRecFillDefault(&rec);

    for (int i = 0; i < Sets.size(); i++)
    {
        if (Sets.at(i).code == spCode)
        {
            rec = Sets.at(i);
            break;
        }
    }

    return rec;
}


void MainImpl::palleteAddNew()
{
    // Добавляем новую запись о породе
    int i;
    int newSpCode = 0;
    SetRec rec;
    PalleteEditImpl dlg(this, Qt::Dialog);

    // find max code
    for (i = 0; i < Sets.size(); i++)
    {
        if (newSpCode < Sets.at(i).code)
        {
            newSpCode = Sets.at(i).code;
        }
    }
    newSpCode++;

    setRecFillDefault(&rec);
    rec.code = newSpCode;

    dlg.setPalleteItem(rec);

    if (dlg.exec())
    {
        rec = dlg.getPalleteItem();
        Sets.append(rec);
        addSetPalleteItem(-1, rec);
        projSaved = false;
    }
}


void MainImpl::palleteAddNewFromData()
{
    // Добавляет умолчательные записи в существующую палитру на основе записей
    // из данных

    int i;

    QList<int> curCodes;
    QList<int> newCodes;

    curCodes.clear();
    newCodes.clear();

    // Получим списко существущих кодов в палитре
    for (i = 0; i < Sets.size(); i++)
    {
        curCodes.append(Sets.at(i).code);
    }

    // Пробежаться по данным, найти и запомнить коды пород, которых нет в палитре
    for (i = 0; i < Data.size(); i++)
    {
        if ( curCodes.indexOf(Data.at(i).spCode) == -1 &&
                newCodes.indexOf(Data.at(i).spCode) == -1 )
        {
            newCodes.append(Data.at(i).spCode);
        }
    }

    // Отсортировать новый список
    std::sort(newCodes.begin(), newCodes.end());

    // Добавить умолчательные пареметры для новых пород
    // Вцелом новый список будет несортированным, для удобства редактирования новых записей
    for (i = 0; i < newCodes.size(); i++)
    {
        SetRec rec;
        setRecFillDefault(&rec);
        rec.code = newCodes.at(i);

        Sets.append(rec);
        addSetPalleteItem(-1, rec);
    }

    if (newCodes.size() > 0)
    {
        QMessageBox mb(QMessageBox::Information,
                       tr("Crowns Pallete"),
                       tr("%1 new pallete item was added.").arg(newCodes.size()),
                       QMessageBox::Ok,
                       this);
        mb.exec();
        projSaved = false;
    }
}

void MainImpl::exportPalleteToFile()
{
    // Сохранить палитру в ini файл
    QString     fn = "";
    fn = QFileDialog::getSaveFileName(this,
                                      tr("Export pallete"),
                                      "",
                                      "Pallete (*.pal);;");
    if (fn.isEmpty())
    {
        return;
    }

    QFileInfo fi(fn);
    if (fi.suffix() != "pal")
    {
        fn += ".pal";
    }

    exportPalleteToIniFile(fn);
}


void MainImpl::importPalleteFromFile()
{
    // Импортируем палитру из файла
    if (!Sets.isEmpty())
    {
        QMessageBox mb(QMessageBox::Warning,
                       tr("Crowns Pallete import"),
                       tr("Pallete already exists. This action will override it. Continue?"),
                       QMessageBox::Yes|QMessageBox::No,
                       this);

        if (mb.exec() == QMessageBox::No)
        {
            return;
        }
    }

    QString     fn = "";
    fn = QFileDialog::getOpenFileName(this,
                                      tr("Import pallete"),
                                      "",
                                      "Pallete (*.pal);;");
    if (fn.isEmpty())
    {
        return;
    }

    importPalleteFromIniFile(fn);
    projSaved = false;
}


int MainImpl::exportPalleteToIniFile(QString fileName)
{
    // Сохраняет палитру, будет как для проекта, так и для экспорта
    QSettings pal(fileName, QSettings::IniFormat);

    pal.beginGroup("Pallete");
    pal.remove("");
    pal.beginWriteArray("PalItem");

    for (int i = 0; i < Sets.size(); i++)
    {
        pal.setArrayIndex(i);
        pal.setValue("code", Sets.at(i).code);
        pal.setValue("trunkColor", Sets.at(i).trunkColor.name());
        pal.setValue("projLineColor", Sets.at(i).projLineColor.name());
        pal.setValue("projFillColor", Sets.at(i).projFillColor.name());
        pal.setValue("projLineStyle", Sets.at(i).projLineStyle);
        pal.setValue("projLineWidth", Sets.at(i).projLineWidth);
        pal.setValue("spName", Sets.at(i).spName);
        pal.setValue("draw", Sets.at(i).draw);
        pal.setValue("isFill", Sets.at(i).isFill);
        pal.setValue("spType", Sets.at(i).spType);
        pal.setValue("projFillStyle", Sets.at(i).projFillStyle);
    }

    pal.endArray();
    pal.endGroup();

    return 1;
}


int MainImpl::importPalleteFromIniFile(QString fileName)
{
    // Читает палитру из конфигурационного файла
    int i;
    QSettings pal(fileName, QSettings::IniFormat);

    pal.beginGroup("Pallete");
    int size = pal.beginReadArray("PalItem");

    Sets.clear();
    clearTable(table_Pallete);

    for (i = 0; i < size; i++)
    {
        pal.setArrayIndex(i);
        SetRec rec;

        rec.code = pal.value("code").toInt();
        rec.trunkColor.setNamedColor( pal.value("trunkColor").toString() );
        rec.projLineColor.setNamedColor( pal.value("projLineColor").toString() );
        rec.projFillColor.setNamedColor( pal.value("projFillColor").toString() );
        rec.projLineStyle = pal.value("projLineStyle").toInt();
        rec.projLineWidth = pal.value("projLineWidth").toInt();
        rec.spName = pal.value("spName").toString();
        rec.draw = pal.value("draw").toBool();
        rec.isFill = pal.value("isFill").toBool();
        rec.spType = pal.value("spType").toInt();
        rec.projFillStyle = pal.value("projFillStyle").toInt();

        Sets.append(rec);
    }

    pal.endArray();
    pal.endGroup();

    std::sort(Sets.begin(), Sets.end(), sortSetsByType);

    for (i = 0; i < Sets.size(); i++)
    {
        addSetPalleteItem(-1, Sets.at(i));
    }

    return 1;
}


void MainImpl::addSetDataItem(int index, DataRec rec)
{
    // Добавляет в таблицу/изменяет поле

    QTableWidgetItem *TreeNum;
    QTableWidgetItem *X;
    QTableWidgetItem *Y;
    QTableWidgetItem *Nx;
    QTableWidgetItem *Ny;
    QTableWidgetItem *Sx;
    QTableWidgetItem *Sy;
    QTableWidgetItem *Wx;
    QTableWidgetItem *Wy;
    QTableWidgetItem *Ex;
    QTableWidgetItem *Ey;
    QTableWidgetItem *D;
    QTableWidgetItem *H;
    QTableWidgetItem *age;
    QTableWidgetItem *spCode;

    int oldIndex = index;

    if (index == -1)
    {
        table_Data->setRowCount(table_Data->rowCount() + 1);
        index = table_Data->rowCount() - 1;

        TreeNum = new QTableWidgetItem();
        X = new QTableWidgetItem();
        Y = new QTableWidgetItem();
        Nx = new QTableWidgetItem();
        Ny = new QTableWidgetItem();
        Sx = new QTableWidgetItem();
        Sy = new QTableWidgetItem();
        Wx = new QTableWidgetItem();
        Wy = new QTableWidgetItem();
        Ex = new QTableWidgetItem();
        Ey = new QTableWidgetItem();
        D = new QTableWidgetItem();
        H = new QTableWidgetItem();
        age = new QTableWidgetItem();
        spCode = new QTableWidgetItem();
    }
    else
    {
        TreeNum = table_Data->item(index, 0);
        X = table_Data->item(index, 1);
        Y = table_Data->item(index, 2);
        Nx = table_Data->item(index, 3);
        Ny = table_Data->item(index, 4);
        Sx = table_Data->item(index, 5);
        Sy = table_Data->item(index, 6);
        Wx = table_Data->item(index, 7);
        Wy = table_Data->item(index, 8);
        Ex = table_Data->item(index, 9);
        Ey = table_Data->item(index, 10);
        D = table_Data->item(index, 11);
        H = table_Data->item(index, 12);
        age = table_Data->item(index, 13);
        spCode = table_Data->item(index, 14);
    }

    TreeNum->setText(QString("%1").arg(rec.TreeNum));
    X->setText(QString("%1").arg(rec.X));
    Y->setText(QString("%1").arg(rec.Y));
    Nx->setText(QString("%1").arg(rec.Nx));
    Ny->setText(QString("%1").arg(rec.Ny));
    Sx->setText(QString("%1").arg(rec.Sx));
    Sy->setText(QString("%1").arg(rec.Sy));
    Wx->setText(QString("%1").arg(rec.Wx));
    Wy->setText(QString("%1").arg(rec.Wy));
    Ex->setText(QString("%1").arg(rec.Ex));
    Ey->setText(QString("%1").arg(rec.Ey));
    D->setText(QString("%1").arg(rec.D));
    H->setText(QString("%1").arg(rec.H));
    age->setText(QString("%1").arg(rec.age));
    spCode->setText(QString("%1").arg(rec.spCode));

    TreeNum->setFlags(TreeNum->flags() ^ Qt::ItemIsEditable);
    X->setFlags(X->flags() ^ Qt::ItemIsEditable);
    Y->setFlags(Y->flags() ^ Qt::ItemIsEditable);
    Nx->setFlags(Nx->flags() ^ Qt::ItemIsEditable);
    Ny->setFlags(Ny->flags() ^ Qt::ItemIsEditable);
    Sx->setFlags(Sx->flags() ^ Qt::ItemIsEditable);
    Sy->setFlags(Sy->flags() ^ Qt::ItemIsEditable);
    Wx->setFlags(Wx->flags() ^ Qt::ItemIsEditable);
    Wy->setFlags(Wy->flags() ^ Qt::ItemIsEditable);
    Ex->setFlags(Ex->flags() ^ Qt::ItemIsEditable);
    Ey->setFlags(Ey->flags() ^ Qt::ItemIsEditable);
    D->setFlags(D->flags() ^ Qt::ItemIsEditable);
    H->setFlags(H->flags() ^ Qt::ItemIsEditable);
    age->setFlags(age->flags() ^ Qt::ItemIsEditable);
    spCode->setFlags(spCode->flags() ^ Qt::ItemIsEditable);

    if (oldIndex == -1)
    {
        table_Data->setItem(index, 0, TreeNum);
        table_Data->setItem(index, 1, X);
        table_Data->setItem(index, 2, Y);
        table_Data->setItem(index, 3, Nx);
        table_Data->setItem(index, 4, Ny);
        table_Data->setItem(index, 5, Sx);
        table_Data->setItem(index, 6, Sy);
        table_Data->setItem(index, 7, Wx);
        table_Data->setItem(index, 8, Wy);
        table_Data->setItem(index, 9, Ex);
        table_Data->setItem(index, 10, Ey);
        table_Data->setItem(index, 11, D);
        table_Data->setItem(index, 12, H);
        table_Data->setItem(index, 13, age);
        table_Data->setItem(index, 14, spCode);
    }
}


void MainImpl::clearTable(QTableWidget *table)
{
    // Очищает таблицу, нужно подумать как тут с памятью быть.
    // По идее достаточно сделать setRowCount(0)
    // TODO: оптимизировать
    //table->clearContents();
    table->setRowCount(0);
    /*
    while (table->rowCount()>0)
    {
        table->removeRow(0);
    }
    */
}


void MainImpl::on_table_Data_cellDoubleClicked(int row, int)
{
    // Редактируем запись данных по двойному клику
    DataEditImpl dlg(this, Qt::Dialog);

    int treeNum = table_Data->item(row, 0)->text().toInt();
    int idx = TreeNums.indexOf(treeNum);
    DataRec rec = Data.at(idx);

    dlg.setDataItem(rec);
    dlg.setSets(Sets);

    if (dlg.exec())
    {
        rec = dlg.getDataItem();
        Data.replace(idx, rec);
        addSetDataItem(row, rec);
        projSaved = false;
    }
}

void MainImpl::dataAddNew()
{
    // Добавляет новувю запись с данными
    DataEditImpl dlg(this, Qt::Dialog);
    int i;
    int newTreeNum = 0;
    DataRec rec;

    memset(&rec, 0, sizeof(rec));

    for (i = 0; i < Data.size(); i++)
    {
        if (newTreeNum < Data.at(i).TreeNum)
        {
            newTreeNum = Data.at(i).TreeNum;
        }
    }
    newTreeNum++;

    rec.TreeNum = newTreeNum;
    dlg.setDataItem(rec);
    dlg.setSets(Sets);

    if (dlg.exec())
    {
        rec = dlg.getDataItem();
        Data.append(rec);
        addSetDataItem(-1, rec);
        projSaved = false;
    }
}


void MainImpl::importDataFromIniFile(QString fileName)
{
    // читаем данные из ini файла
    QSettings dat(fileName, QSettings::IniFormat);

    Data.clear();
    TreeNums.clear();
    clearTable(table_Data);

    dat.beginGroup("Data");
    int size = dat.beginReadArray("DataItem");

    for (int i = 0; i < size; i++)
    {
        DataRec rec;

        dat.setArrayIndex(i);
        rec.TreeNum = dat.value("TreeNum").toInt();
        rec.X   = dat.value("X").toDouble();
        rec.Y   = dat.value("Y").toDouble();
        rec.Nx  = dat.value("Nx").toDouble();
        rec.Ny  = dat.value("Ny").toDouble();
        rec.Sx  = dat.value("Sx").toDouble();
        rec.Sy  = dat.value("Sy").toDouble();
        rec.Wx  = dat.value("Wx").toDouble();
        rec.Wy  = dat.value("Wy").toDouble();
        rec.Ex  = dat.value("Ex").toDouble();
        rec.Ey  = dat.value("Ey").toDouble();
        rec.D   = dat.value("D").toDouble();
        rec.H   = dat.value("H").toDouble();
        rec.age = dat.value("Age").toDouble();
        rec.spCode = dat.value("spCode").toInt();

        Data.append(rec);
        addSetDataItem(-1, rec);
        TreeNums.append(rec.TreeNum);
    }

    dat.endArray();
    dat.endGroup();

}


void MainImpl::exportDataFile()
{
    // Сохраняем данные в файл
    QString filter;
    QString fileName;
    QFileInfo fi;
    TxtDataFormat format = FORMAT_V3;

    QString filter_delim = ";;";
    QString filter_txt1   = "Data in TXT version 1 (*.txt)";
    QString filter_txt2   = "Data in TXT version 2 / with Height (*.txt)";
    QString filter_txt3   = "Data in TXT version 3 / with Age (*.txt)";
    QString filter_crown  = "Data in native format (*.crown)";
    QString filter_all    = "All files (*)";

    QFileDialog dlg(this, tr("Export data file"), "",
                    filter_txt3  + filter_delim +
                    filter_txt2  + filter_delim +
                    filter_txt1  + filter_delim +
                    filter_crown + filter_delim +
                    filter_all);

    dlg.setAcceptMode(QFileDialog::AcceptSave);

    if (!dlg.exec())
    {
        return;
    }

    if (dlg.selectedFiles().size() < 1)
    {
        return;
    }

    fileName = dlg.selectedFiles().at(0);
    filter   = dlg.selectedNameFilter();

    fi.setFile(fileName);

    if (filter == filter_crown)
    {
        if (fi.suffix() != "crown")
        {
            fileName += ".crown";
        }
        exportDataToIniFile(fileName);
    }
    else
    {
        if (filter != filter_all && fi.suffix() != "txt")
        {
            fileName += ".txt";
        }

        if (filter == filter_txt1)
            format = FORMAT_V1;
        else if (filter == filter_txt2)
            format = FORMAT_V2;

        exportDataToTxtFile(fileName, format);
    }
}


int MainImpl::exportDataToTxtFile(QString fileName, TxtDataFormat format)
{
    // экспорт данных в текстовый файл
    QFile data_file;
    QList<DataRec> tmp = Data;

    std::sort(tmp.begin(), tmp.end(), sortDataCompareTreeNum);

    data_file.setFileName(fileName);
    if (data_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&data_file);

        out << "\n"; // Первую строчку не будем забивать
        out << tmp.size() << "\n"; // количество деревьев

        for (int i = 0; i < tmp.size(); i++)
        {
            DataRec rec = tmp.at(i);

            out     << rec.TreeNum << "\t"
                    << rec.X << "\t"
                    << rec.Y << "\t"
                    << rec.Nx << "\t"
                    << rec.Ny << "\t"
                    << rec.Sx << "\t"
                    << rec.Ny << "\t"
                    << rec.Wx << "\t"
                    << rec.Wy << "\t"
                    << rec.Ex << "\t"
                    << rec.Ey << "\t"
                    << rec.D  << "\t";

            if (format >= FORMAT_V2)
                out << rec.H << "\t";

            if (format >= FORMAT_V3)
                out << rec.age << "\t";

            out     << rec.spCode
                    << "\n";
        }
    }
    data_file.close();

    return 1;
}


int MainImpl::exportDataToIniFile(QString fileName)
{
    // Экспортирует данные в формате ini
    QSettings dat(fileName, QSettings::IniFormat);

    QList<DataRec> tmp = Data;
    std::sort(tmp.begin(), tmp.end(), sortDataCompareTreeNum);

    dat.beginGroup("Data");
    dat.remove("");
    dat.beginWriteArray("DataItem");

    for (int i = 0; i < tmp.size(); i++)
    {
        DataRec rec = tmp.at(i);

        dat.setArrayIndex(i);
        dat.setValue("TreeNum",        rec.TreeNum);
        dat.setValue("X",       (qreal)rec.X);
        dat.setValue("Y",       (qreal)rec.Y);
        dat.setValue("Nx",      (qreal)rec.Nx);
        dat.setValue("Ny",      (qreal)rec.Ny);
        dat.setValue("Sx",      (qreal)rec.Sx);
        dat.setValue("Sy",      (qreal)rec.Sy);
        dat.setValue("Wx",      (qreal)rec.Wx);
        dat.setValue("Wy",      (qreal)rec.Wy);
        dat.setValue("Ex",      (qreal)rec.Ex);
        dat.setValue("Ey",      (qreal)rec.Ey);
        dat.setValue("D",       (qreal)rec.D);
        dat.setValue("H",       (qreal)rec.H);
        dat.setValue("Age",     (qreal)rec.age);
        dat.setValue("spCode",         rec.spCode);
    }

    dat.endArray();
    dat.endGroup();

    return 1;
}

void MainImpl::on_button_MMeasure_clicked(bool checked)
{
    // Изменяет состояние поведения кликов мышки в области просмотра
    measureActions(StateMeasureDistance, checked);
}

void MainImpl::on_button_MCut_clicked(bool checked)
{
    // Изменяет состояние поведения кликов мышки в области просмотра
    measureActions(StateCut, checked);
}

void MainImpl::drawCut()
{
    // Рисуем срез
    float Nx, Sx, Wx, Ex;
    float Ny, Sy, Wy, Ey;
    float X, Y;
    float D, H;

    int spCode;
    int TreeNum;

    float cutWidth = getPlateWidth();
    float cutHeight = 50;

    // глубина среза +-
    float deep = getCutDeep();

    float scale = getScaleFactor();

    int i;

    QList<DataRec> wrk;
    wrk.clear();

    float shiftP = getPlateShapeWidth();
    float work_y = getCutCoord().y();

    // Пробежаться по деревьям в выборке среза, заодно сформировать список для
    // последующего цикла
    for (i = 0; i < Data.size(); i++)
    {
        float tmp_y = (Data.at(i).Y + shiftP) * scale;
        float tmp_x = (Data.at(i).X + shiftP) * scale;

        qDebug() << " Tree: " << Data.at(i).TreeNum
                  << " tmp_y = "   << tmp_y
                  << " work_y = " << work_y
                  << " deep = "   << deep
                  << " x = " << tmp_x
                  << " y = " << tmp_y;

        if ((tmp_y >= (work_y - deep*scale)) &&
                (tmp_y <= (work_y + deep*scale)))
        {
            
            wrk.append(Data.at(i));
            if (Data.at(i).H > cutHeight)
            {
                cutHeight = Data.at(i).H;
            }

            if (Data.at(i).X > cutWidth)
            {
                cutWidth = Data.at(i).X + shiftP;
            }
        }
    }

    qDebug() << "Tree count: " << wrk.size();

    int gvW, gvH;

    gvW = (cutWidth + 2*shiftP)*scale;
    gvH = (cutHeight + 2*shiftP)*scale;

    QPen   pen, oldPen;
    QBrush brush, oldBrush;

    sceneCut->clear();
    sceneCut->setSceneRect(0, 0, gvW, gvH);
    sceneCut->setBackgroundBrush(getProbePlateColor());

    std::sort(wrk.begin(), wrk.end(), sortDataCompareYCoord);

    zValue = 1;
    for (i = 0; i < wrk.size(); i++)
    {
        DataRec dat = wrk.at(i);
        
        MathPrepare(&dat);        
        
        TreeNum = dat.TreeNum;
        X = dat.X;
        Y = dat.Y;
        Nx = dat.Nx;
        Ny = dat.Ny;
        Sx = dat.Sx;
        Sy = dat.Sy;
        Wx = dat.Wx;
        Wy = dat.Wy;
        Ex = dat.Ex;
        Ey = dat.Ey;
        spCode = dat.spCode;
        H = dat.H;
        D = dat.D;


        X  = (X + shiftP) * scale;
        Y  = (Y + shiftP) * scale;
        Nx = X + Nx * scale;
        Ny = Y - Ny * scale;
        Sx = X + Sx * scale;
        Sy = Y + Sy * scale;
        Wx = X - Wx * scale;
        Wy = Y + Wy * scale;
        Ex = X + Ex * scale;
        Ey = Y + Ey * scale;

        qDebug() << "X = " << X << " Y = " << Y;

        float x1 = Wx;
        float x2 = X - D/100/2*scale;
        float x3 = X;
        float x4 = Ex;

        float y1 = gvH - shiftP*scale;
        float y2 = y1 - H/4*scale;
        float y3 = y1 - H*scale;

        SetRec rec = getSetRec(spCode);

        if (rec.isFill)
        {
            brush.setColor(rec.projFillColor);
            brush.setStyle((Qt::BrushStyle)rec.projFillStyle);
        }

        pen.setColor(rec.projLineColor);

        switch (rec.spType)
        {
            case TYPE_CONIFER:
            {
                // отрисовка елок
                QPolygonF polygon;
                polygon << QPointF(x1, y2)
                        << QPointF(x3, y3)
                        << QPointF(x4, y2)
                        << QPointF(x1, y2);
                QGraphicsPolygonItem *pol_it;

                pol_it = sceneCut->addPolygon(polygon, pen, brush);
                pol_it->setZValue(zValue);
                pol_it->setToolTip(genTreeToolTip(wrk.at(i), rec));

                break;
            }

            case TYPE_LEAFY:
            {
                // Отрисовка лиственных
                QRectF rect(x1, y3, x4-x1, 1.5*H*scale);
                int startA = 0;
                int spanA  = 180;

                QGraphicsPathItem *path_it;

                path_it = sceneCut->addPath(chordPath(rect, startA, spanA), pen, brush);
                path_it->setZValue(zValue);
                path_it->setToolTip(genTreeToolTip(wrk.at(i), rec));

                break;
            }
        }

        pen = oldPen;
        brush = oldBrush;
        
        pen.setColor(rec.trunkColor);
        pen.setWidth(3);
        brush.setColor(rec.trunkColor);
        if (rec.code != -1)
        {
            brush.setStyle((Qt::BrushStyle)1);
        }
        QGraphicsRectItem *rect_it;
        rect_it = sceneCut->addRect(x2, y2, D/100*scale, H/4*scale, pen, brush);
        rect_it->setZValue(zValue);
        rect_it->setToolTip(genTreeToolTip(wrk.at(i), rec));
        zValue++;
        
        pen = oldPen;
        brush = oldBrush;
    }

    pen = oldPen;
    pen.setWidth(getBorderWidth());
    sceneCut->addRect(shiftP*scale, shiftP*scale, cutWidth*scale, cutHeight*scale, pen);

    drawGrid(sceneCut, cutWidth, cutHeight, shiftP);
    drawBorderMarks(sceneCut, cutWidth, cutHeight, shiftP, 2);

    graphicsView_Cut->setScene(sceneCut);

    cutW = cutWidth;
    cutH = cutHeight;
}


void MainImpl::on_button_RedrawCut_clicked()
{
    if (cutCoord.x() != -1)
    {
        drawCut();
    }
}

void MainImpl::on_button_ZoomInCut_clicked()
{
    // Zoom in
    if (graphicsView_Cut->transform().m11() < pow(1.1, 10))
    {
        graphicsView_Cut->scale(1.1, 1.1);
    }

}

void MainImpl::on_button_ZoomOutCut_clicked()
{
    // Zoom out
    if (graphicsView_Cut->transform().m11() > pow(0.9, 10))
    {
        graphicsView_Cut->scale(0.9, 0.9);
    }
}

void MainImpl::on_button_ZoomNoCut_clicked()
{
    // Zoom 1:1
    graphicsView_Cut->resetTransform();
}

void MainImpl::on_button_ZoomFitCut_clicked()
{
    // Zoom fit
    graphicsView_Cut->fitInView(0, 0, sceneCut->width(), sceneCut->height(), Qt::KeepAspectRatio);
}

void MainImpl::exportCutImg()
{
    // Экспортируем картинку со срезом
    int w = (cutW + 2*getPlateShapeWidth()) * getScaleFactor() * 2;
    int h = (cutH + 2*getPlateShapeWidth()) * getScaleFactor() * 2;

    exportToImg(graphicsView_Cut->scene(), w, h);
}


void MainImpl::drawAll()
{
    // Перерисовать все
    drawPlot();
    drawCut();
}


void MainImpl::drawGrid(QGraphicsScene *s, float w, float h, float shiftP)
{
    // рисуем сетку из основных и промежуточных линий
    int gridCount;
    QPen pen, oldPen;
    QBrush brush, oldBrush;
    int scale = getScaleFactor();

    float x,y,x1,x2,y1,y2;
    int i;

    // Промежуточная сетка
    if (getGridInterStep() > 0)
    {
        pen.setWidthF(0.25);
        pen.setColor(getColorGridInter());

        // Вертикальные линии
        gridCount = w / getGridInterStep();
        y1 = shiftP*scale;
        y2 = (shiftP + h)*scale;
        for (i = 1; i < gridCount; i++) // Нулевую рисовать не нужно
        {
            x = (shiftP + i*getGridInterStep())*scale;
            s->addLine(x, y1, x, y2, pen);
        }

        // Горизонтальные линии
        gridCount = h / getGridInterStep();
        x1 = shiftP*scale;
        x2 = (shiftP + w)*scale;
        for (i = 1; i < gridCount; i++) // Нулевую рисовать не нужно
        {
            y = (shiftP + i*getGridInterStep())*scale;
            s->addLine(x1, y, x2, y, pen);
        }
    }

    // Основная сетка
    if (getGridMainStep() > 0)
    {
        pen.setWidthF(0.5);
        pen.setColor(getColorGridMain());

        // Вертикальные линии
        gridCount = w / getGridMainStep();
        y1 = shiftP*scale;
        y2 = (shiftP + h)*scale;
        for (i = 1; i < gridCount; i++) // Нулевую рисовать не нужно
        {
            x = (shiftP + i*getGridMainStep())*scale;
            s->addLine(x, y1, x, y2, pen);
        }

        // Горизонтальные линии
        gridCount = h / getGridMainStep();
        x1 = shiftP*scale;
        x2 = (shiftP + w)*scale;
        for (i = 1; i < gridCount; i++) // Нулевую рисовать не нужно
        {
            y = (shiftP + i*getGridMainStep())*scale;
            s->addLine(x1, y, x2, y, pen);
        }
    }
}


void MainImpl::drawBorderMarks(QGraphicsScene *s, float w, float h, float shiftP, int flag)
{
    // рисуем отметки на границе
    // если flag == 1, подразумевается полотно с проекциями крон, в остальных
    // случаях - полотно среза
    int x,y,x1,x2,x3,x4,y1,y2,y3,y4;
    QPen pen, oldPen;
    QBrush brush, oldBrush;
    QFont font, oldFont;
    int scale = getScaleFactor();

    int i;

    pen.setWidth(2); // TODO: подумать как задавать, пока жезезно
    pen.setColor(getColorPlateBorder());

    // Отсечки по горизонтали
    if (getMarksStep() > 0)
    {
        int marksCountW = w / getMarksStep();
        int markSize = 10; // TODO: подумать как задавать, пока железно
        QGraphicsTextItem *txt;

        y1 = shiftP*scale;
        y2 = shiftP*scale - markSize;
        y3 = (h + shiftP)*scale;
        y4 = (h + shiftP)*scale + markSize;

        font = getMarksFont();
        //s->setFont(font);
        QFontMetrics fm(font);
        QString numer;

        for (i = 0; i < marksCountW; i++)
        {
            x  = (shiftP + i*getMarksStep())*scale;

            if (flag == 1)
            {
                s->addLine(x, y1, x, y2, pen);
            }

            s->addLine(x, y3, x, y4, pen);

            if (getIsMarksNumeration())
            {
                numer = QString("%1").arg(i*getMarksStep());
                txt = s->addText(numer, font);
                //txt->setTextWidth(fm.width(numer));
                //txt->setTextw`

                if (flag == 1)
                {
                    txt->setPos(x  - txt->boundingRect().width() / 2,
                                y1 - txt->boundingRect().height() - 5);
                }
                else
                {
                    txt->setPos(x  - txt->boundingRect().width() / 2,
                                y4 + 5);
                }
            }
        }

        x = (shiftP + w)*scale;
        if (flag == 1)
            s->addLine(x, y1, x, y2, pen);

        s->addLine(x, y3, x, y4, pen);
        if (getIsMarksNumeration())
        {
            numer = QString("%1").arg(w);
            txt = s->addText(numer, font);
            //txt->setTextWidth(fm.width(numer));

            if (flag == 1)
            {
                txt->setPos(x  - txt->boundingRect().width() / 2,
                            y1 - txt->boundingRect().height() - 5);
            }
            else
            {
                txt->setPos(x  - txt->boundingRect().width() / 2,
                            y4 + 5);
            }
        }

        // Отсечки по вертикали
        int marksCountH = h / getMarksStep();

        x1 = shiftP*scale;
        x2 = shiftP*scale - markSize;
        x3 = (w + shiftP)*scale;
        x4 = (w + shiftP)*scale + markSize;

        for (i = 0; i < marksCountH; i++)
        {
            if (flag == 1)
                y = (shiftP + i*getMarksStep())*scale;
            else
                y = (shiftP + h - i*getMarksStep())*scale;

            s->addLine(x1, y, x2, y, pen);

            if (flag == 1)
                s->addLine(x3, y, x4, y, pen);

            if (getIsMarksNumeration())
            {
                numer = QString("%1").arg(i*getMarksStep());
                txt = s->addText(numer, font);
                //txt->setTextWidth(fm.width(numer));
                txt->setPos(x1 - txt->boundingRect().width() - markSize - 2,
                            y  - txt->boundingRect().height()/2);
            }
        }

        if (flag == 1)
            y = (shiftP + h)*scale;
        else
            y = shiftP*scale;

        s->addLine(x1, y, x2, y, pen);

        if (flag == 1)
            s->addLine(x3, y, x4, y, pen);

        if (getIsMarksNumeration())
        {
            numer = QString("%1").arg(h);
            txt = s->addText(numer, font);

            txt->setPos(x1 - txt->boundingRect().width() - markSize - 5,
                        y  - txt->boundingRect().height()/2);
        }
    }
}


QString MainImpl::genTreeToolTip(DataRec dat, SetRec set)
{
    // Генерирует тултип для дерева
    return tr("%1. %2\nD = %3, H = %4, Type = %5\nAge: %6 year(s)")
            .arg(dat.TreeNum)
            .arg(set.spName)
            .arg(dat.D)
            .arg(dat.H)
            .arg(set.code)
            .arg(dat.age);
}


void MainImpl::on_spinBox_Transparent_valueChanged(int v)
{
    // новое значение полупрозрачности
    transVal = v;
    projSaved = false;
}

void MainImpl::on_spinBox_CutDeep_valueChanged(double     v)
{
    // новое значение глубины среза
    cutDeep = v;
    projSaved = false;
}

void MainImpl::setLayouts(uint val)
{
    // отображаемые слои

    checkBox_SectoringLayout->setChecked(val & LAYOUT_SECTORING);
    checkBox_TrunkLayout->setChecked(val & LAYOUT_TRUNKS);
    checkBox_CrownLayout->setChecked(val & LAYOUT_CROWNS);
}


void MainImpl::setSortType(SortType val)
{
    // Устанавливает тип сортировки
    switch (val)
    {
        case SORT_HEIGHT:
        {
            radioButton_HeightSort->setChecked(true);
            break;
        }

        case SORT_DIAMETER:
        {
            radioButton_DiameterSort->setChecked(true);
            break;
        }
    }
}


void MainImpl::setRestrictions(uint val)
{
    // устанавливает выбор ограничений

    checkBox_HeightRestriction->setChecked(val & REST_HEIGHT);
    checkBox_DiameterRestriction->setChecked(val & REST_DIAMETER);
    checkBox_AgeRestriction->setChecked(val & REST_DIAMETER);
}


void MainImpl::setRestValues(RestValues val)
{
    // устанавливает  значения ограничений по отображению
    lineEdit_HeightFrom->setText(QString("%1").arg(val.height_min));
    lineEdit_HeightTo->setText(QString("%1").arg(val.height_max));
    lineEdit_DiameterFrom->setText(QString("%1").arg(val.diameter_min));
    lineEdit_DiameterTo->setText(QString("%1").arg(val.diameter_max));
    lineEdit_AgeFrom->setText(QString("%1").arg(val.age_min));
    lineEdit_AgeTo->setText(QString("%1").arg(val.age_max));
}


void MainImpl::setDefaults()
{
    // Задаёт умолчательные настройки для органов уплавления

    QFont default_font;
    RestValues rest = RestValues();
    rest.height_max   = 100;
    rest.diameter_max = 100;

    /* Восстанавливаем настройки */
    setLayouts(LAYOUT_ALL);        // Отображаемые слои
    setSortType(SORT_HEIGHT);      // тип сортировки
    setRestrictions(REST_NONE);    // виды ограничений на отображение
    setRestValues(rest);           // значения ограничений
    setProbePlateColor(Qt::white); // Цвет поля отрисовки
    setPlateWidth(20);             // Ширина пробной площади
    setPlateHeight(20);            // Высота пробной площади
    setPlateShapeWidth(4);         // Пустое поле вокруг пробной площади
    setIsClearCrowns(false);       // Очишать поле за пределами пробной площади
    setIsPlateBorderDisplay(true); // Показывать границы пробной площади
    setBorderWidth(3);             // Толщина границ пробной площади
    setColorPlateBorder(Qt::black);// Цвет границы пробной площади
    setIsMarksShow(true);          // Рисовать отметки на полях
    setMarksStep(2);               // Шаг отметок на осях
    setIsMarksNumeration(true);    // Нумеровать отметки на осях
    setMarksFont(default_font);    // Шрифт для отрисовки меток
    setIsGridShow(true);           // Рисовать сетку
    setGridMainStep(2);            // Шаг основной сетки
    setGridInterStep(1);           // Шаг промежуточной сетки
    setColorGridMain(Qt::darkGray);       // Цвет основной сетки
    setColorGridInter(Qt::lightGray);     // Цвет промежуточной сетки
    setTrunkView(TRUNK_VIEW_CIRCLE);      // Установить вид отображения стволов
    setTrunkDiameter(TRUNK_DIAMETER_TYPO);// Установить вид диаметра ствола
    setTrunkDiameterTypoSize(3);          // Установить условный диаметр ствола
    setIsTrunkNumerationShow(true);       // Установить нумерацию стволов деревьев
    setTrunkFont(default_font);           // Установить фришт нумерации стволов
    setIsCrownNodesShow(true);            // Показывать отметки на профиле кроны
    setCrownNodesSize(4);                 // Размер отметок на профиле кроны
    setScaleFactor(40);                   // Сколько точек в метре
    //setMouseState(MOUSE_NONE);            // Поведение мыши в окне просмотра графики
    setAppState(StateNone);
    setMeasureStart(QPointF(-1, -1));
    setMeasureEnd(QPointF(-1, -1));
    setCutCoord(QPointF(-1, -1));
    setTransVal(50);                      // полупрозрачность заливки крон
    setCutDeep(1);                        // глубина среза

    Data.clear();
    setBackgroundImageFromFile("");

    cutW = 0;
    cutH = 0;
    zValue = 0;

    projSaved = false;
    projFileName = "";
    setTitle("No Name");
}


void MainImpl::projectNew()
{
    // Новый проект
    if (checkProjectSave() == -1)
        return;

    setDefaults(); // Делаем установки
    drawAll();
}


void MainImpl::projectOpen()
{
    // открыть проект
    if (checkProjectSave() == -1)
        return;

    QString     fn = "";
    fn = QFileDialog::getOpenFileName(this,
                                      tr("Open project"),
                                      "",
                                      "Crowns project (*.cproj);;");
    if (fn.isEmpty())
    {
        return;
    }

    QFileInfo fi(fn);

    projFileName = fn;
    importProjectFromIniFile(fn);
    importPalleteFromIniFile(fn);
    importDataFromIniFile(fn);
    projSaved = true; 

    setTitle(fi.baseName());
    drawAll();
}


void MainImpl::projectSave()
{
    // Сохранить проект
    if (projFileName == "")
    {
        projectSaveAs();
    }
    else
    {
        exportProjectToIniFile(projFileName);
        exportPalleteToIniFile(projFileName);
        exportDataToIniFile(projFileName);
        projSaved = true;
    }
}


void MainImpl::projectSaveAs()
{
    // Сохранить проект как
    QString     fn = "";
    fn = QFileDialog::getSaveFileName(this,
                                      tr("Save project"),
                                      "",
                                      "Crowns project (*.cproj);;");
    if (fn.isEmpty())
    {
        return;
    }

    QFileInfo fi(fn);
    if (fi.suffix() != "cproj")
    {
        fn += ".cproj";
    }
    fi.setFile(fn);

    projFileName = fn;
    exportProjectToIniFile(projFileName);
    exportPalleteToIniFile(projFileName);
    exportDataToIniFile(projFileName);
    projSaved = true;

    setTitle(fi.baseName());
}


int MainImpl::exportProjectToIniFile(QString fileName)
{
    // Экспортирует данные в конфигурационный файл
    QSettings dat(fileName, QSettings::IniFormat);
    QColor color;

    dat.beginGroup("Project");
    dat.remove("");

    dat.setValue("Layouts", getLayouts()); // Отображаемые слои
    dat.setValue("SortType", getSortType()); // тип сортировки
    dat.setValue("Restrictions", getRestrictions()); // виды ограничений на отображение

    dat.setValue("RestValueDF", getRestValues().diameter_min); // значения ограничений
    dat.setValue("RestValueDT", getRestValues().diameter_max);
    dat.setValue("RestValueHF", getRestValues().height_min);
    dat.setValue("RestValueHT", getRestValues().height_max);
    dat.setValue("RestValueAF", getRestValues().age_min);
    dat.setValue("RestValueAT", getRestValues().age_max);

    dat.setValue("ProbePlateColor", getProbePlateColor().name()); // Цвет поля отрисовки
    dat.setValue("PlateWidth", getPlateWidth()); // Ширина пробной площади
    dat.setValue("PlateHeight", getPlateHeight()); // Высота пробной площади
    dat.setValue("PlateShapeWidth", getPlateShapeWidth()); // Пустое поле вокруг пробной площади
    dat.setValue("IsClearCrowns", getIsClearCrowns()); // Очишать поле за пределами пробной площади
    dat.setValue("IsPlateBorderDisplay", getIsPlateBorderDisplay()); // Показывать границы пробной площади
    dat.setValue("BorderWidth", getBorderWidth()); // Толщина границ пробной площади
    dat.setValue("ColorPlateBorder", getColorPlateBorder().name()); // Цвет границы пробной площади
    dat.setValue("IsMarksShow", getIsMarksShow()); // Рисовать отметки на полях
    dat.setValue("MarksStep", getMarksStep()); // Шаг отметок на осях
    dat.setValue("IsMarksNumeration", getIsMarksNumeration()); // Нумеровать отметки на осях
    dat.setValue("MarksFont", getMarksFont().toString()); // Шрифт для отрисовки меток
    dat.setValue("IsGridShow", getIsGridShow()); // Рисовать сетку
    dat.setValue("GridMainStep", getGridMainStep()); // Шаг основной сетки
    dat.setValue("GridInterStep", getGridInterStep()); // Шаг промежуточной сетки
    dat.setValue("ColorGridMain", getColorGridMain().name()); // Цвет основной сетки
    dat.setValue("ColorGridInter", getColorGridInter().name()); // Цвет промежуточной сетки
    dat.setValue("TrunkView", getTrunkView()); // Установить вид отображения стволов
    dat.setValue("TrunkDiameter", getTrunkDiameter()); // Установить вид диаметра ствола
    dat.setValue("TrunkDiameterTypoSize", getTrunkDiameterTypoSize()); // Установить условный диаметр ствола
    dat.setValue("IsTrunkNumerationShow", getIsTrunkNumerationShow()); // Установить нумерацию стволов деревьев
    dat.setValue("TrunkFont", getTrunkFont().toString()); // Установить фришт нумерации стволов
    dat.setValue("IsCrownNodesShow", getIsCrownNodesShow()); // Показывать отметки на профиле кроны
    dat.setValue("CrownNodesSize", getCrownNodesSize()); // Размер отметок на профиле кроны
    dat.setValue("ScaleFactor", getScaleFactor()); // Сколько точек в метре
    //dat.setValue("MouseState", getMouseState()); // Поведение мыши в окне просмотра графики
    dat.setValue("AppState", appState); // Состояние диалога

    //dat.setValue("MeasureStart", getMeasureStart()); //
    //dat.setValue("MeasureEnd", getMeasureEnd()); //
    //dat.setValue("CutCoord", getCutCoord()); //

    dat.setValue("TransVal", getTransVal()); // полупрозрачность заливки крон
    dat.setValue("CutDeep", getCutDeep()); // глубина среза

    // Подложка
    dat.setValue("BackgroundImage", background_image);

    dat.endGroup();

    return 1;
}


int MainImpl::importProjectFromIniFile(QString fileName)
{
    // читаем настройки из файла
    QSettings dat(fileName, QSettings::IniFormat);
    QFont font;
    RestValues rest = RestValues();

    dat.beginGroup("Project");

    setLayouts(dat.value("Layouts").toInt()); // Отображаемые слои
    setSortType((SortType)dat.value("SortType").toInt()); // тип сортировки
    setRestrictions(dat.value("Restrictions").toInt()); // виды ограничений на отображение

    rest.height_min   = dat.value("RestValueHF").toInt();
    rest.height_max   = dat.value("RestValueHT").toInt();
    rest.diameter_min = dat.value("RestValueDF").toInt();
    rest.diameter_max = dat.value("RestValueDT").toInt();
    rest.age_min      = dat.value("RestValueAF").toInt();
    rest.age_max      = dat.value("RestValueAT").toInt();
    setRestValues(rest); // значения ограничений

    setProbePlateColor(QColor(dat.value("ProbePlateColor").toString())); // Цвет поля отрисовки

    setPlateWidth(dat.value("PlateWidth").toInt()); // Ширина пробной площади
    setPlateHeight(dat.value("PlateHeight").toInt()); // Высота пробной площади
    setPlateShapeWidth(dat.value("PlateShapeWidth").toInt()); // Пустое поле вокруг пробной площади
    setIsClearCrowns(dat.value("IsClearCrowns").toBool()); // Очишать поле за пределами пробной площади
    setIsPlateBorderDisplay(dat.value("IsPlateBorderDisplay").toBool()); // Показывать границы пробной площади
    setBorderWidth(dat.value("BorderWidth").toInt()); // Толщина границ пробной площади

    setColorPlateBorder(QColor(dat.value("ColorPlateBorder").toString())); // Цвет границы пробной площади

    setIsMarksShow(dat.value("IsMarksShow").toBool()); // Рисовать отметки на полях
    setMarksStep(dat.value("MarksStep").toInt()); // Шаг отметок на осях
    setIsMarksNumeration(dat.value("IsMarksNumeration").toBool()); // Нумеровать отметки на осях

    font.fromString(dat.value("MarksFont").toString());
    setMarksFont(font); // Шрифт для отрисовки меток

    setIsGridShow(dat.value("IsGridShow").toBool()); // Рисовать сетку
    setGridMainStep(dat.value("GridMainStep").toInt()); // Шаг основной сетки
    setGridInterStep(dat.value("GridInterStep").toInt()); // Шаг промежуточной сетки

    setColorGridMain(QColor(dat.value("ColorGridMain").toString())); // Цвет основной сетки
    setColorGridInter(QColor(dat.value("ColorGridInter").toString())); // Цвет промежуточной сетки

    setTrunkView((TrunkView)dat.value("TrunkView").toInt()); // Установить вид отображения стволов
    setTrunkDiameter((TrunkDiameter)dat.value("TrunkDiameter").toInt()); // Установить вид диаметра ствола
    setTrunkDiameterTypoSize(dat.value("TrunkDiameterTypoSize").toInt()); // Установить условный диаметр ствола
    setIsTrunkNumerationShow(dat.value("IsTrunkNumerationShow").toBool()); // Установить нумерацию стволов деревьев

    font.fromString(dat.value("TrunkFont").toString());
    setTrunkFont(font); // Установить шрифт нумерации стволов

    setIsCrownNodesShow(dat.value("IsCrownNodesShow").toBool()); // Показывать отметки на профиле кроны
    setCrownNodesSize(dat.value("CrownNodesSize").toInt()); // Размер отметок на профиле кроны
    setScaleFactor(dat.value("ScaleFactor").toInt()); // Сколько точек в метре
    //setMouseState((MouseState)dat.value("MouseState").toInt()); // Поведение мыши в окне просмотра графики /* Deprecated */
    setAppState((int)dat.value("AppState").toInt()); // Внутренее состояние диалога

    //setMeasureStart(dat.value("MeasureStart").toInt()); //
    //setMeasureEnd(dat.value("MeasureEnd").toInt()); //
    //setCutCoord(dat.value("CutCoord").toInt()); //

    setTransVal(dat.value("TransVal").toInt()); // полупрозрачность заливки крон
    setCutDeep(dat.value("CutDeep").toInt()); // глубина среза

    // Подложка
    background_image = dat.value("BackgroundImage").value<QImage>();
    if (!background_image.isNull())
    {
        line_BackgroundImagePath->setText("[internal]");
    }

    dat.endGroup();

    return 1;
}


void MainImpl::on_checkBox_SectoringLayout_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_checkBox_TrunkLayout_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_checkBox_CrownLayout_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_radioButton_HeightSort_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_radioButton_DiameterSort_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_checkBox_HeightRestriction_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_lineEdit_HeightFrom_textEdited(QString )
{
    // TODO
    projSaved = false;
}

void MainImpl::on_lineEdit_HeightTo_textEdited(QString )
{
    // TODO
    projSaved = false;
}

void MainImpl::on_checkBox_DiameterRestriction_clicked(bool)
{
    // TODO
    projSaved = false;
}

void MainImpl::on_lineEdit_DiameterFrom_textEdited(QString )
{
    // TODO
    projSaved = false;
}

void MainImpl::on_lineEdit_DiameterTo_textEdited(QString )
{
    // TODO
    projSaved = false;
}

void MainImpl::setTitle(QString str)
{
    setWindowTitle("Crowns - " + str);
}


int MainImpl::checkProjectSave()
{
    // Проверяет что проект сохранен, спрашивает что делать. если нет
    //  0 - после проверки так и не сохранен (нажат save, при попытке сохранения нажата Отмена)
    // -1 - продолжить работу (Cancel)
    //  1 - сохранен
    //  2 - нажато сохранить, но сохранения не произошло
    if (projSaved == false)
    {
        switch (QMessageBox::question(this,
                                      tr("Project not saved"),
                                      tr("Changes in project does not saved, save?"),
                                      QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel))
        {
            case QMessageBox::Yes:
                projectSave();
                if (projSaved == false)
                {
                    return 2;
                }

                break;

            case QMessageBox::Cancel:
                return -1;
                break;

            default:
                return 0;
        }
    }

    return 1;
}


void MainImpl::closeEvent(QCloseEvent * ev)
{
    // Обработка закрытия приложения
    if (checkProjectSave() == -1)
    {
        ev->ignore();
    }
    else
    {
        ev->accept();
    }
}


void MainImpl::about()
{
    // О программе
    QMessageBox::about(this, tr("About Crowns"),
                       tr("<h2>About Crowns</h2>"
                          "<p>Version: " APP_VERSION_FULL "</p>"
                          "<p>Application for drawing crowns projections and cut building.</p>\n"
                          "<p>This programm distributed under GPLv2 terms. Full license text "
                          "see in LICENSE file</p>\n"
                          "<p>Site: <a=http://hatred.homelinux.net>http://hatred.homelinux.net</a></p>\n"
                          "<p>Icon taked from: http://www.iconspedia.com/dload.php?up_id=3301</p>"
                          "<p>Copyright 2009 Alexander 'hatred' Drozdov</p>"
                          "<p><strong>Authors:</strong></p>"
                          "<p>Alexander Drozdov - programming, help text</p>"
                          "<p>Anna Vozmischeva - coordiantor, help text</p>"
                          "<p>Thanks to Alexander Omelko for idea and Delphi implementation</p>"));
}


void MainImpl::help()
{
    // Короткая справка
    QString page = qApp->applicationDirPath() + "/doc/index.html";
    HelpBrowser::showPage(page);
}

void MainImpl::setAppState(int stateNew)
{
    // TODO
    int stateOld = appState;
    
    switch(stateNew)
    {
        case StateNone:
        {
            if(stateOld == StateMeasureArea)
            {
                area->clear();
            }
        }
        case StateMeasureDistance:
        case StateCut:
        case StateMeasureAreaDraw:
        case StateMeasureArea:
        {
            button_MMeasure->setChecked(false);
            button_MCut->setChecked(false);
            button_MArea->setChecked(false);
            
            if(stateNew == StateMeasureDistance)
            {
                button_MMeasure->setChecked(true);
            }
            
            if(stateNew == StateCut)
            {
                button_MCut->setChecked(true);
            }
            
            if(stateNew == StateMeasureAreaDraw ||
               stateNew == StateMeasureArea)
            {
                button_MArea->setChecked(true);
                if(stateNew == StateMeasureAreaDraw)
                {
                    area->start();
                }
            }
            break;
        }
        
        default:
        {
           /**/    
        }
    }

    appState = stateNew;    
}


void MainImpl::customEvent(QEvent *ev)
{
    // TODO
    CustomEvent *e = (CustomEvent *)ev;
    int subType = e->subType();

    switch(appState)
    {
        case StateMeasureArea:
        {
            if(subType == ceMousePress ||
               subType == ceMouseMove  ||
               subType == ceMouseRelease)
            {
                struct MoveElementData *data = (struct MoveElementData*)(e->data().constData());
                // TODO                 
                
                if(e->subType() == ceMousePress)
                {
                    area->updatePoint(data->pos, StartMovePoint);
                }
                else if(e->subType() == ceMouseMove)
                {
                    area->updatePoint(data->pos, ProcessMovePoint);
                }
                else if(e->subType() == ceMouseRelease)
                {
                    area->updatePoint(data->pos, StopMovePoint);
                }
                
                qreal size = area->getAreaSize();
                //label->setNum(size);
                label_Info->setText(tr("Area size: %1 squre meters.").arg(size));
            }
            break;
        }
        
        case StateMeasureAreaDraw:
        {
            struct MoveElementData *data = (struct MoveElementData*)e->data().constData();
            if(subType == ceMousePress)
            {
                if(data->button == Qt::RightButton)
                {
                    area->end();
                    //state = StateMeasure;
                    setAppState(StateMeasureArea);
                }
                else
                {
                    area->addBasePoint(data->pos);
                    area->redraw();
                    qDebug() << "Add dot: " << data->pos.x() << ", " << data->pos.y();
                }
                qreal size = area->getAreaSize();
                //label->setNum(size);
                label_Info->setText(tr("Area size: %1 squre meters.").arg(size));
                
            }
            else if(subType == ceMouseMove)
            {
                area->drawLine(data->pos);
            }
            break;
        }
        
        case StateMeasureDistance:
        {
            struct MoveElementData *data = (struct MoveElementData*)(e->data().constData());

            if(data->button != Qt::LeftButton ||
               e->subType() != ceMousePress)
            {
                break;
            }

            qDebug() << "StateMeasureDistance: "
                    << data->pos.x() << ", " << data->pos.y() << "; ";

            if (measureStart.x() == -1)
            {
                setMeasureStart(data->pos);
            }
            else
            {
                setMeasureEnd(data->pos);

                qreal dx = measureEnd.x() - measureStart.x();
                qreal dy = measureEnd.y() - measureStart.y();
                qreal ln = 0;

                if (getScaleFactor() != 0)
                {
                    ln = sqrt(dx*dx + dy*dy) / getScaleFactor();
                }

                /*
                QMessageBox mb(QMessageBox::Information,
                               tr("Measure distance"),
                               tr("Distance is %1 meters.").arg(ln),
                               QMessageBox::Ok,
                               this);
                mb.exec();
                */
                
                label_Info->setText(tr("Distance: %1 meters.").arg(ln));
                log.add(tr("Distance from (%1, %2) to (%3, %4): %5 meters.")
                       .arg(measureStart.x() / getScaleFactor())
                       .arg(measureStart.y() / getScaleFactor())
                       .arg(measureEnd.x() / getScaleFactor())
                       .arg(measureEnd.y() / getScaleFactor())
                       .arg(ln));

                setMeasureStart(QPointF(-1, -1));
                setMeasureEnd(QPointF(-1, -1));
            }

            break;
        }

        case StateCut:
        {
            struct MoveElementData *data = (struct MoveElementData*)(e->data().constData());

            if(data->button != Qt::LeftButton ||
               e->subType() != ceMousePress)
            {
                break;
            }
            
            qDebug() << "CutEvent";
            
            setCutCoord(data->pos);
            drawCut();
            setAppState(StateNone);
            break;
        }
    }
    
    ev->accept();
}


void MainImpl::on_button_MArea_clicked(bool checked)
{
    // TODO
    // Измерение площадей
    measureActions(StateMeasureAreaDraw, checked);

}

void MainImpl::measureActions(int state, bool checked)
{
    // TODO
    // идентичные события для нажатия кнопок замеров и построения среза
    if (checked == true)
    {
        //setMouseState(MOUSE_CUT);
        setAppState(state);
        return;
    }
    else if (appState != StateNone)
    {
        //setMouseState(MOUSE_NONE);
        setAppState(StateNone);
    }

    setMeasureStart(QPointF(-1, -1));
    setMeasureEnd(QPointF(-1, -1));
    setCutCoord(QPointF(-1, -1));
}

/**
  * Общая часть для экспорта в графический формат
  */
void MainImpl::exportToImg(QGraphicsScene *s, int width, int height)
{
    QString img_name;
    QString img_filter;

    QString filter_delim = ";;";
    QString filter_png   = "PNG (*.png)";
    QString filter_svg   = "SVG (*.svg)";

    QFileDialog img_save_dlg(this, tr("Save image"), "",
                             filter_png + filter_delim +
                             filter_svg);

    if (!img_save_dlg.exec())
    {
        return;
    }

    if (img_save_dlg.selectedFiles().size() == 0)
    {
        return;
    }

    img_name   = img_save_dlg.selectedFiles().at(0);
    img_filter = img_save_dlg.selectedNameFilter();

    QFileInfo fi(img_name);

    if(img_filter == filter_svg)
    {
        if(fi.suffix() != "svg")
            img_name += ".svg";

        qDebug() << "Export to SVG";
        exportToSvg(img_name, s, width, height);
    }
    else /* if (img_filter == filter_png) */ // По умолчанию - PNG
    {
        if(fi.suffix() != "png")
            img_name += ".png";

        qDebug() << "Export to PNG";
        exportToPng(img_name, s, width, height);
    }
}

/**
  * Рендеринг в PNG
  *
  * @param s      - сцена
  * @param width  - ширина области
  * @param height - высота области
  */
void MainImpl::exportToPng(QString file_name, QGraphicsScene *s, int width, int height)
{
    QPainter img_painter;
    QPixmap  img_data(width, height);
    const char *img_format = "png";

    img_painter.begin(&img_data);

    s->render(&img_painter,
              QRectF(0, 0, width,   height),
              QRectF(0, 0, width/2, height/2));

    img_painter.end();
    img_data.save(file_name, img_format);
}

/**
  * Рендеринг в SVG
  *
  * @param s      - сцена
  * @param width  - ширина области
  * @param height - высота области
  */
void MainImpl::exportToSvg(QString file_name, QGraphicsScene *s, int width, int height)
{
    QPainter img_painter;
    QSvgGenerator svg_generator;
    QFileInfo fi(projFileName);

    svg_generator.setFileName(file_name);
    svg_generator.setSize(QSize(width, height));
    svg_generator.setViewBox(QRect(0, 0, width, height));
    svg_generator.setTitle(fi.baseName() + QString(" exported SVG image"));
    //svg_generator.setDescription("");

    img_painter.begin(&svg_generator);

    s->render(&img_painter,
              QRectF(0, 0, width,   height),
              QRectF(0, 0, width/2, height/2));

    img_painter.end();
}

void MainImpl::on_button_BackgroundImagePath_clicked()
{
    // TODO
    QString img_name;
    //QString img_filter;

    //QString filter_delim = ";;";
    QString filter_png   = "PNG (*.png)";
    //QString filter_svg   = "SVG (*.svg)";

    QFileDialog img_load_dlg(this, tr("Load image"), "",
                             filter_png /* + filter_delim + */);

    if (!img_load_dlg.exec())
    {
        return;
    }

    if (img_load_dlg.selectedFiles().size() == 0)
    {
        return;
    }

    img_name = img_load_dlg.selectedFiles().at(0);
    setBackgroundImageFromFile(img_name);
    drawPlot();
    projSaved = false;
}

void MainImpl::on_backgroundImageRemove_clicked()
{
    setBackgroundImageFromFile("");
    drawPlot();
    projSaved = false;
}

void MainImpl::setBackgroundImageFromFile(QString file_name)
{
    if (file_name.isEmpty())
    {
        background_image = QImage();
        line_BackgroundImagePath->setText("");
    }
    else
    {
        background_image.load(file_name);
        line_BackgroundImagePath->setText(file_name);
    }
}

void MainImpl::exportProfileOctave()
{
    QString octave_name;
    QString filter_math   = "Octave/Scilab/Mathlab (*.m *.oct *.sce *.sci)";

    QFileDialog octave_save_dlg(this, tr("Save Octave"), "",
                             filter_math);

    if (Data.size() == 0)
    {
        return;
    }

    if (!octave_save_dlg.exec())
    {
        return;
    }

    if (octave_save_dlg.selectedFiles().size() == 0)
    {
        return;
    }

    octave_name   = octave_save_dlg.selectedFiles().at(0);

    QFileInfo fi(octave_name);
    if (fi.suffix().isEmpty())
    {
        octave_name += ".m";
    }

    if (fi.exists())
    {
        int btn = QMessageBox::question(this,
                                        tr("File exists"),
                                        QString("File '%1' exists, override it?").arg(octave_name),
                                        QMessageBox::Yes|QMessageBox::No,
                                        QMessageBox::No);

        if (btn == QMessageBox::No)
        {
            return;
        }
    }

    QFile octave_file;

    octave_file.setFileName(octave_name);
    if (!octave_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return;
    }

    QTextStream out(&octave_file);

    float Nx, Sx, Wx, Ex;
    float Ny, Sy, Wy, Ey;
    float X, Y;
    float D, H;

    int spCode;
    int TreeNum;

    out << "base_latitude  = 0\n";
    out << "base_longitude = 0\n";

    QString trees_cmd;
    QString plot_cmd = "plot(\n";

    for (int i = 0; i < Data.size(); i++)
    {
        DataRec dat = Data.at(i);

        MathPrepare(&dat);

        TreeNum = dat.TreeNum;
        X = dat.X;
        Y = dat.Y;
        Nx = dat.Nx;
        Ny = dat.Ny;
        Sx = dat.Sx;
        Sy = dat.Sy;
        Wx = dat.Wx;
        Wy = dat.Wy;
        Ex = dat.Ex;
        Ey = dat.Ey;
        spCode = dat.spCode;
        H = dat.H;
        D = dat.D;

        // Перерасчет сторон крон в абсолютные величины, а не относитель ствола
        Nx = X + Nx;
        Ny = Y - Ny; // Север вверху, для отрисовки на площади, другие знаки
        Sx = X + Sx;
        Sy = Y + Sy; // Юг внизу, для отрисовки на площади, другие знаки
        Wx = X - Wx;
        Wy = Y + Wy;
        Ex = X + Ex;
        Ey = Y + Ey;

        QList<QPointF> crown_xy;

        crown_xy << QPointF(Wx, Wy)
                 << QPointF(Sx, Sy)
                 << QPointF(Ex, Ey)
                 << QPointF(Nx, Ny);

        double accuracy = 0.1;

        // Выводим имя переменной и координаты ствола
        trees_cmd += QString("tree_trunk%1 = [%2, %3]\n")
                        .arg(TreeNum)
                        .arg(X)
                        .arg(getPlateHeight() - Y);

        // Выводим начальную точку
        trees_cmd += QString("tree_crown%1 = [\n\t%2, %3;\n")
                        .arg(TreeNum)
                        .arg(crown_xy.at(0).x())
                        .arg(getPlateHeight() - crown_xy.at(0).y());


        for (int j = 0; j < crown_xy.count(); j++)
        {
            QPointF p0;
            QPointF p1;
            QPointF c0; // Первая контрольная точка
            QPointF c1; // Вторая контрольная точка

            p0 = crown_xy[j];
            if (j == crown_xy.count() - 1) // в общем случае 4
            {
                p1 = crown_xy[0];
            }
            else
            {
                p1 = crown_xy[j + 1];
            }

            calcControlPoints(p0, p1, c0, c1);

            // расчет параметров кривых безье
            for (double t = accuracy; t < 1.0; t += accuracy)
            {
                double x = getBezierXY(p0.x(), c0.x(), c1.x(), p1.x(), t);
                double y = getBezierXY(p0.y(), c0.y(), c1.y(), p1.y(), t);

                y = getPlateHeight() - y; //Переворачиваем для системы координат с (0,0) слава-снизу

                trees_cmd += QString("\t%1, %2;\n").arg(x).arg(y);
            }
        }

        trees_cmd += "]\n";

        plot_cmd += QString("\ttree_trunk%1(1), tree_trunk%1(2), tree_crown%1(:,1), tree_crown%1(:,2)").arg(TreeNum);
        if (i != (Data.size() - 1))
        {
            plot_cmd += ", ";
        }
        plot_cmd += "\n";
    }

    plot_cmd += ");\n";

    if (fi.suffix() == "m" || fi.suffix() == "oct")
    {
        // Насколько я знаю, axis() специфична только для Octave
        // в Scilab запускается графическое окно, в котором можно задать настройки осей
        plot_cmd += QString("axis([%1,%2,%3,%4]);\n")
                        .arg(-getPlateShapeWidth())
                        .arg(getPlateWidth() + getPlateShapeWidth())
                        .arg(-getPlateShapeWidth())
                        .arg(getPlateHeight() + getPlateShapeWidth());
    }

    out << trees_cmd << "\n\n" << plot_cmd;

    octave_file.close();
}

