#ifndef LOGGER_H
#define LOGGER_H
//
#include <QTextEdit>
//

typedef enum _LoggerClass {
    LOG_NONE = 0,
    LOG_FILE = (1 << 0),
    LOG_QTEXTEDIT = (1 << 1)
} LoggerClass;

typedef enum _LogType {
    LOG_INFO,
    LOG_ERROR,
    LOG_WARNING
} LogType;
//
class Logger  
{
private:
    QTextEdit * textEdit;
    int logClass;

public:
    void add(QString line, LogType type = LOG_INFO);
    void setTextEdit(QTextEdit * value);
    QTextEdit * getTextEdit();
    int getLogClass();
    void setLogClass(int log_class);
	Logger(int log_class = LOG_NONE);
    
};
#endif
