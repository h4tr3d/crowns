#include "logger.h"
//
Logger::Logger( int log_class) 
{
	// TODO
    setLogClass(log_class);
    setTextEdit(NULL);
}
//

int Logger::getLogClass()
{
	return logClass;
}

void Logger::setLogClass(int value)
{
	logClass = value;
}

QTextEdit * Logger::getTextEdit()
{
    return textEdit;
}


void Logger::setTextEdit(QTextEdit * value)
{
    textEdit = value;
}


void Logger::add(QString line, LogType type )
{
    if((logClass & LOG_QTEXTEDIT) && (textEdit != NULL))
    {
        textEdit->append(line);    
    }
}

