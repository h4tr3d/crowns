#ifndef AREAMEASURE_H
#define AREAMEASURE_H
//
#include <QDialog>
#include <QGraphicsView>
#include <QPoint>
#include <QGraphicsEllipseItem>
//
#include <math.h>
#include <iostream>
//

const int ctrlSize = 4;

typedef enum {
    StartMovePoint,
    ProcessMovePoint,
    StopMovePoint
} UpdatePointStatus;

class AreaMeasure  
{

public:
    void drawLine(QPointF p);
    void setAccuracy(qreal value) { accuracy = value; }
    qreal getAccuracy() { return accuracy; }
    void setScale(qreal value) { scale = value; }
    qreal getScale() { return scale; }
    qreal getAreaSize() { return areaSize; }
	void updatePoint(QPointF pos, UpdatePointStatus s);
    void end();
    void start();
    void update();
    void updatePointByIndex(int index, QPointF p);
    void clear();
    void redraw();
    void addBasePoint(QPointF p);
    AreaMeasure(QGraphicsScene *s);

private:
    qreal accuracy;
    void calcArea();
    qreal scale;
    qreal areaSize;
    QVector<QPointF> basePoints;
    QVector<QPointF> pathPoints;
    
    QVector<QGraphicsEllipseItem *> controlItem;
    QGraphicsPathItem              *pathItem;
    QGraphicsPathItem              *pathControlItem;
    QGraphicsLineItem              *draw_line;
    
    QGraphicsScene *scene;
    bool isEnd;
};
#endif
