#ifndef MYGRAPHICSSCENE_H
#define MYGRAPHICSSCENE_H
//
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>

#include <iostream>

#include "customevent.h"

//
// Наш класс основанный на QGraphicsScene
// пока только переоределили функцию отлавливающую события от мышки
// нужно на будущеее для оперделения координаты нажатия и постоения 
// среза. На данный момент, основному окну пересылается сообщение с
// данными о нажатии
//
class MyGraphicsScene : public QGraphicsScene
{
Q_OBJECT
protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *ev);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *ev);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *ev);

private:
	void sendMouseEvent(int type, QGraphicsSceneMouseEvent *ev);
    QWidget       *parent;
    
public:
	MyGraphicsScene(QWidget *win);
	
};
#endif
