#ifndef __COMMON_H__
#define __COMMON_H__

#include <QString>
#include <QColor>
#include <QPainterPath>
#include <math.h>

typedef struct _setRec {
    int code;
    QColor trunkColor;
    QColor projLineColor;
    QColor projFillColor;
    int    projFillStyle;
    int    projLineStyle;
    int    projLineWidth;
    QString spName;
    bool draw;
    bool isFill;
    int  spType;
} SetRec;

typedef struct _dataRec {
    int   TreeNum;
    float X;
    float Y;
    float Nx;
    float Ny;
    float Sx;
    float Sy;
    float Wx;
    float Wy;
    float Ex;
    float Ey;
    float D;
    float H;
    float age;
    int   spCode;
} DataRec;

/* Какие слои активны */
typedef enum _layouts {
    LAYOUT_NONE      = 0,
    LAYOUT_SECTORING = (1 << 0),
    LAYOUT_TRUNKS    = (1 << 1),
    LAYOUT_CROWNS    = (1 << 2),
    
    LAYOUT_ALL = LAYOUT_SECTORING | LAYOUT_TRUNKS | LAYOUT_CROWNS
} Layouts;

/* виды сортировки */
typedef enum _sorting {
    SORT_HEIGHT,
    SORT_DIAMETER
} SortType;

/* ограничения */
typedef enum _Restrictions {
    REST_NONE     = 0,
    REST_HEIGHT   = (1 << 0),
    REST_DIAMETER = (1 << 1),
    REST_AGE      = (1 << 2),
    
    REST_ALL = REST_HEIGHT | REST_DIAMETER | REST_AGE
} RestrictionType;

typedef struct _rest_values {
    uint height_min;   // высота от
    uint height_max;   // высота до
    uint diameter_min; // диаметр от
    uint diameter_max; // диаметр до
    uint age_min;      // возраст от
    uint age_max;      // возраст до
} RestValues;

/* Вид отображения стволов */
typedef enum _TrunkView {
    TRUNK_VIEW_CIRCLE,
    TRUNK_VIEW_PALLETE = 0xFE
} TrunkView;

/* как понимать размер ствола */
typedef enum _TrunkDiameter {
    TRUNK_DIAMETER_REAL,
    TRUNK_DIAMETER_TYPO
} TrunkDiameter;

/* Поведение мыши в окне проссмотра графики */
typedef enum _MouseState {
    MOUSE_NONE,
    MOUSE_MEASURE,
    MOUSE_CUT
} MouseState;

/* Тип породы */
typedef enum _TreeType {
    TYPE_NONE,
    TYPE_CONIFER = 1,
    TYPE_LEAFY
} TreeType;

/**
  Формат текстового файла с данными (выбирается при импорте/экспорте)
 */
typedef enum _TxtDataFormat {
    FORMAT_V1, /// Первый вариант, исходный:            TreeNum/X/Y/Nx/Ny/Sx/Sy/Wx/Wy/Ex/Ey/spCode
    FORMAT_V2, /// Второй вариант, добавляется диаметр: TreeNum/X/Y/Nx/Ny/Sx/Sy/Wx/Wy/Ex/Ey/D/spCode
    FORMAT_V3  /// Третий вариант, добавлен возраст:    TreeNum/X/Y/Nx/Ny/Sx/Sy/Wx/Wy/Ex/Ey/D/age/spCode
} TxtDataFormat;
/*****************************************************************************/
#define MAX(x, y) (((x) > (y))?(x):(y))
#define MIN(x, y) (((x) < (y))?(x):(y))

/* Знак переменной, без хаков */
#define SIGN(x) ( (x >= 0) ? (1) : (-1) )

bool sortDataCompareHeight(DataRec &rec1, DataRec &rec2);
bool sortDataCompareDiameter(DataRec &rec1, DataRec &rec2);
bool sortDataCompareTreeNum(DataRec &rec1, DataRec &rec2);
bool sortDataCompareYCoord(DataRec &rec1, DataRec &rec2);
bool sortSetsByType(SetRec &rec1, SetRec &rec2);

void setRecFillDefault(SetRec *rec);

// for drawing
QPainterPath piePath(QRectF rect, float startA, float spanA);
QPainterPath arcPath(QRectF rect, float startA, float spanA);
QPainterPath chordPath(QRectF rect, float startA, float spanA);

void MathPrepare(DataRec *rec);

// Расчёт параметров кривой Безье
qreal getBezierXY(qreal xy0, qreal xy_ctrl0, qreal xy_ctrl1, qreal xy1, qreal t);

#endif // __COMMON_H__
