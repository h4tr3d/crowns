#ifndef MYGRAPHICSVIEWCOORDEVENT_H
#define MYGRAPHICSVIEWCOORDEVENT_H
//
#include <QApplication>
#include <QEvent>
//
enum _myEvents {
    MyCoordEvent = QEvent::User + 1
};
//
class MyGraphicsViewCoordEvent : public QEvent
{
//Q_OBJECT

private:
    qreal y;
    qreal x;
public:
    void setY(qreal value) { y = value; }
    qreal getY() { return y; }
    void setX(qreal value) { x = value; }
    qreal getX() { return x; }
	MyGraphicsViewCoordEvent();
	
};
#endif
