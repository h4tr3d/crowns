#include "mygraphicsscene.h"
//
MyGraphicsScene::MyGraphicsScene( QWidget *win ) 
	: QGraphicsScene()
{
    // TODO
    parent = win;
}
//

void MyGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *ev)
{    
    sendMouseEvent(ceMousePress, ev);
    QGraphicsScene::mousePressEvent(ev);
    
    std::cout << "Mouse Press: "
              << ev->scenePos().x()
              << ", "
              << ev->scenePos().y()
              << std::endl;
}

void MyGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *ev)
{
    sendMouseEvent(ceMouseMove, ev);       
    QGraphicsScene::mouseMoveEvent(ev);
}

void MyGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *ev)
{
    sendMouseEvent(ceMouseRelease, ev);
    QGraphicsScene::mouseReleaseEvent(ev);
}

void MyGraphicsScene::sendMouseEvent(int type, QGraphicsSceneMouseEvent *ev)
{
    // TODO
    CustomEvent *e = new CustomEvent(type);
    struct MoveElementData ptr;
    ptr.pos    = ev->scenePos();
    ptr.button = ev->button();
    e->setData(&ptr, sizeof(ptr));
    QApplication::postEvent((QObject*)parent, (QEvent*)e);
}

