#ifndef DATAEDITIMPL_H
#define DATAEDITIMPL_H
//
#include <QDialog>
#include <QList>
#include "ui_dataedit.h"
#include "common.h"
//
class DataEditImpl : public QDialog, public Ui::DataEdit
{
Q_OBJECT
private:
    DataRec dataItem;
    QList<SetRec> sets;
    
    
public:
    void setSets(QList<SetRec> s);
    void setDataItem(DataRec &value);
    DataRec getDataItem() { return dataItem; }
    DataEditImpl( QWidget * parent, Qt::WindowFlags f );

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_spin_X_valueChanged(double );
    void on_spin_Y_valueChanged(double );
    void on_spin_Nx_valueChanged(double );
    void on_spin_Ny_valueChanged(double );
    void on_spin_Sx_valueChanged(double );
    void on_spin_Sy_valueChanged(double );
    void on_spin_Wx_valueChanged(double );
    void on_spin_Wy_valueChanged(double );
    void on_spin_Ex_valueChanged(double );
    void on_spin_Ey_valueChanged(double );
    void on_spin_D_valueChanged(double );
    void on_spin_H_valueChanged(double );
    void on_spin_spCode_valueChanged(int );
    void on_lineEdit_TreeNum_textChanged(QString );
    void on_spin_A_valueChanged(int );
};
#endif
