#include "palleteeditimpl.h"
//
PalleteEditImpl::PalleteEditImpl( QWidget * parent, Qt::WindowFlags f ) 
	: QDialog(parent, f)
{
    setupUi(this);
	// TODO
    
    // Установим значения по умолчанию    
    SetRec rec;
    setRecFillDefault(&rec);
    setPalleteItem(rec);
}
//

void PalleteEditImpl::on_checkBox_Show_clicked()
{
    palleteItem.draw = checkBox_Show->isChecked();
}

void PalleteEditImpl::on_lineEdit_Name_textChanged(QString text)
{
    palleteItem.spName = text;
}

void PalleteEditImpl::on_checkBox_CrownFill_clicked()
{
    palleteItem.isFill = checkBox_CrownFill->isChecked();
}

void PalleteEditImpl::on_spinBox_LineWidth_valueChanged(int val)
{
    palleteItem.projLineWidth = val;
}

void PalleteEditImpl::on_button_ColorTrunk_clicked()
{
    QColorDialog color_dlg(palleteItem.trunkColor, this);
    
    if(!color_dlg.exec())
    {
        return;
    }
    
    palleteItem.trunkColor = color_dlg.selectedColor();
    int r,g,b;
    palleteItem.trunkColor.getRgb(&r, &g, &b);
    button_ColorTrunk->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
}

void PalleteEditImpl::on_button_ColorCrown_clicked()
{
    QColorDialog color_dlg(palleteItem.projFillColor, this);
    
    if(!color_dlg.exec())
    {
        return;
    }
    
    palleteItem.projFillColor = color_dlg.selectedColor();
    int r,g,b;
    palleteItem.projFillColor.getRgb(&r, &g, &b);
    button_ColorCrown->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));

}

void PalleteEditImpl::on_button_ColorBorder_clicked()
{
    QColorDialog color_dlg(palleteItem.projLineColor, this);
    
    if(!color_dlg.exec())
    {
        return;
    }
    
    palleteItem.projLineColor = color_dlg.selectedColor();
    int r,g,b;
    palleteItem.projLineColor.getRgb(&r, &g, &b);
    button_ColorBorder->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));
}

void PalleteEditImpl::on_buttonBox_accepted()
{
    done(1);
}

void PalleteEditImpl::on_buttonBox_rejected()
{
    done(0);
}

void PalleteEditImpl::on_spinBox_FillStyle_valueChanged(int v)
{
    int w, h;
    w = label_FillView->width();
    h = label_FillView->height();
    
    QPixmap pix(w, h);
    pix.fill(Qt::white);
    QPainter p(&pix);
    
    QBrush brush = p.brush();
    brush.setColor(palleteItem.projFillColor);
    brush.setStyle((Qt::BrushStyle)v);
    p.setBrush(brush);
    p.drawRect(0,0,w-1,h-1);
    
    label_FillView->setPixmap(pix);
    
    palleteItem.projFillStyle = v;
}

void PalleteEditImpl::setPalleteItem(SetRec v)
{
    int r,g,b;
    palleteItem = v;
    
    checkBox_Show->setChecked(v.draw);
    lineEdit_Name->setText(v.spName);
    
    v.trunkColor.getRgb(&r, &g, &b);
    button_ColorTrunk->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));

    v.projFillColor.getRgb(&r, &g, &b);
    button_ColorCrown->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));

    v.projLineColor.getRgb(&r, &g, &b);
    button_ColorBorder->setStyleSheet(QString("background-color: rgb(%1, %2, %3)").arg(r).arg(g).arg(b));

    checkBox_CrownFill->setChecked(v.isFill);
    spinBox_FillStyle->setValue(v.projFillStyle);
    spinBox_LineWidth->setValue(v.projLineWidth);
    
    combo_TreeType->setCurrentIndex(v.spType - 1); // spType numer from 1!!!
}


void PalleteEditImpl::on_combo_TreeType_currentIndexChanged(int index)
{
    // TODO
    palleteItem.spType = index + 1;
}
