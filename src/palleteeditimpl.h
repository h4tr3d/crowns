#ifndef PALLETEEDITIMPL_H
#define PALLETEEDITIMPL_H
//
#include <QColorDialog>
#include <QPainter>

#include "ui_palleteedit.h"

#include "common.h"
//
class PalleteEditImpl : public QDialog, public Ui::PalleteEdit
{
Q_OBJECT
private:
    SetRec palleteItem;
    
    int crownFillStyle;
    QColor colorBorder;
    QColor colorCrown;
    QColor colorTrunk;
    int lineWidth;
    bool crownFill;
    QString itemName;
    bool itemShow;
    
public:
    void setPalleteItem(SetRec value);
    SetRec getPalleteItem() { return palleteItem; }
    
    PalleteEditImpl(QWidget * parent, Qt::WindowFlags f);
    
private slots:
    void on_combo_TreeType_currentIndexChanged(int index);
    void on_spinBox_FillStyle_valueChanged(int );
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_button_ColorTrunk_clicked();
    void on_button_ColorCrown_clicked();
    void on_button_ColorBorder_clicked();
    void on_spinBox_LineWidth_valueChanged(int );
    void on_checkBox_CrownFill_clicked();
    void on_lineEdit_Name_textChanged(QString );
    void on_checkBox_Show_clicked();	
};
#endif
