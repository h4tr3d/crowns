#ifndef CUSTOMEVENT_H
#define CUSTOMEVENT_H
//
#include <QEvent>
#include <QApplication>
//
enum _customEvent {
      myCustomEvent = QEvent::User + 1
};
//
enum customEventTypes {
    ceUnknown,
        
    ceMousePress,
    ceMouseMove,
    ceMouseRelease
};
//

struct MoveElementData {
    QPointF pos;
    Qt::MouseButton button;
};

//
class CustomEvent : public QEvent
{
//Q_OBJECT
public:
	CustomEvent(int type, void *data = NULL, uint size = 0);

public:
    void setData(void *value, uint size)
    {
        evData.clear();
        evData.append((const char *)value, size);
    }
    QByteArray data() { return evData; }
    void setSubType(int value) { eventType = value; }
    int subType() { return eventType; }    

private:
    QByteArray evData;
    int eventType;
};
#endif
