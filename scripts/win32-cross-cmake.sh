#!/bin/bash

source project.conf

mkdir ../build-win32 > /dev/null 2>&1
cd ../build-win32

rm CMakeCache.txt > /dev/null 2>&1
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake/win32-x-mingw32.cmake ..

echo "*** Now you can type 'cd ../build-win32 && make' for build"
