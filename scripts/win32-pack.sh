#!/bin/bash

source project.conf

cd ..
if [ -z "$_APP_VERSION" ]; then
  VERSION=`cat src/version.h | grep '^#[ \t]*define' | sed 's|^#[ \t]*define|#define|' | grep 'APP_VERSION' | head -1 | awk '{print $3}' | sed 's/"//g'`
else
  VERSION=$_APP_VERSION
fi

[ -z "$VERSION" ] && VERSION="$project_default_version"

target=$project-$VERSION-win32
rm -rf $target > /dev/null 2>&1
mkdir $target
cp -a $additional_files $target

if [ -e "build-win32/release/$project.exe" ]; then
  cp -a build-win32/release/$project.exe $target
elif [ -e "build-win32/src/$project.exe" ]; then
  cp -a build-win32/src/$project.exe $target
fi

cp $MINGW32_DLL $target
cp $DLL $target

# Qt DLLs
for lib in $QT_DLL
do
  cp $QT_LIB_PREFIX/$lib $target
done

# Qt Plugins
for plugin in $QT_PLUGINS
do
  subdir=`dirname $plugin`
  if [ "$subdir" != "$plugin" ]; then
    test ! -d $subdir && mkdir -p $target/$subdir
  else
    subdir=""
  fi
  cp $QT_PLUGINS_PREFIX/$plugin $target/$subdir
done

cwd=`pwd`
zip -r ../$target.zip $target
