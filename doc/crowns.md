Авторы:
* *Alexander Drozdov* <adrozdoff@gmail.com>
* *Anna Vozmischeva* <vozmishcheva@inbox.ru>

Это руководство описывает версию 0.3.0 Crowns. В более новых версиях могут быть небольшие изменения и улучшения, но фундаментальных отличий нет. В противном случае, это руководство будет модифицировано :)

Разрешено копировать, распространять и/или модифицировать данный документ на условиях лицензии GNU Free Documentation License, версия 1.1 или более поздней версии, опубликованной Фондом Свободного ПО (Free Software Foundation). Полный текст лицензии доступен на сайте Free Software Foundation.

Август 2009 (с исправлениями от Сентября 2018)

# Введение в Crowns

Приложение для графического построения пробных площадей и замеров на них. Идея создания программы принадлежит Возмищевой Анне, основана на идеях воплощенных в программе Lepeshki, написанной Александром Омелько (Delphi, Windows). При создании делался упор на возможность кроссплатформенного использования приложения, устранение некоторых ограничений, реализация дополнительного функционала.

* Используя данное приложение, вы можете:
* Загружать данные по замерам на пробных площадях и делать их отрисовку;
* Манипулировать параметрами отрисовки;
* Делать замеры дистанций и площадей;
* Создавать срезы;
* Экспортировать результаты построений в графические форматы;
* Сохранять и загружать проекты, экспортировать части проекта для последующего использования.

# Системные требования

Для работы программы достаточно системы на базе процессора частотой от 500МГц, 128Мб оперативной памяти (ограничено только размерами данные в проекте), 25Мб свободного места на жестком диске (без учета размера проектов), при использовании разделяемых библиотек Qt4/Qt5, сама программа занимает меньше 1Мб, монитор любой, с разрешением от 1024х600 (экран типичного на данный момент нетбука), видеокарта любая, манипулятор типа мышь или аналогичный.

Работа программы проверялась в среде Arch Linux, Ubuntu, Windows XP, Windows 8 и 10. Сборка производилась в системе Arch Linux, для Windows XP - путем кросс-компиляции. В дальнейшем планируется переход на независимую сборку при использовании MXE.

# Установка

Программа распространяется в виде исходных текстов, кроме того для среды Windows предоставляется бинарная сборка, выполненная путем кросскомпиляции.

Программу можно свободно скачать с официального сайта:
* https://gitlab.com/h4tr3d/crowns/tags
или
* https://htrd.su/wiki/proekty/crowns/download

Для использования бинарной сборки нужно скачать файл вида
* crowns-*VERSION*-win32.zip

Архив содержит саму программу, текст лицензии, справочное руководство и необходимые для запуска dll-файлы библиотеки Qt.

Для установки программы необходимо распаковать один их этих архивов в любую доступную для записи директорию и запустить файл crowns.exe

Для установки программы из исходных текстов необходимо скачать архив исходных кодов:
* crowns-v *VERSION*.tar.bz2 (возможны варианты: tar.gz, tar и zip)

Кроме того, в системе должны стоять компилятор gcc, утилита make и библиотека Qt, версии не ниже 4.2.

Для сборки программы выполните команды:
```
qmake
make
```    

Для запуска программы перейдите в директорию `bin/` и выполните команду
```
./crowns
```

Установка не требуется. Что бы иметь возможность открыть HTML справку из программы, скопируйте каталог `doc/` в каталог `bin/`.

# Интерфейс приложения

Интерфейс программы построен с использованием библиотеки Qt4/Qt5, выполнен в виде единого окна, разделённого на области, объединенные по смыслу на вкладках. Дополнительные диалоги используются для ввода и редактирование данных для построений, редактирования и добавления палитры. Кроме того используются стандартные диалоги выбора шрифта, цвета, открытия (открытие проекта, загрузка данных, палитры) и сохранения (сохранение проекта, экспорт построений в графический файл, экспорт палитры и данных).

## Главное окно

Сразу после запуска программы откроется главное окно Crowns, содержащее [Строку главного меню](#меню), [Панель настроек](#панель-настроек) и [Рабочая область](#рабочая-область).

![Main Window](/doc/index1.png)


### Меню

![Главное меню](/doc/main_menu.png)

Главное меню содержит пункты:

#### Файл

![Файл](/doc/menu_file.png)


* Новый проект - создать новый проект, если текущий был изменен и не сохранен, программа выдаст предупреждение об этом.
* Открыть проект - открыть ранее сохраненный проект.
* Сохранить проект - сохранить изменения в проекте на жесткий диск.
* Сохранить проект как - сохранить проект под новым именем.
* Выход - выход из программы.

#### Данные

![Данные](/doc/menu_data.png)

* Добавить - добавить строку данных
* Импорт из файла - импортировать данные из текстового файла
* Экспорт в файл - экспорт данных в текстовый или ini файл


#### Палитра 

![Палитра](/doc/menu_pallete.png)

* Добавить - добавить новую строчку данных для палитры.
* Генерировать из данных - на основе пород из таблицы данных генерирует стандартную палитру, которую после можно отредактировать.
* Добавить из данных - для тех пород, которые есть в таблице данных, но нет в палитре, генерирует стандартные записи в палитре.
* Импорт палитры из файла - импортирует палитру из ini файла.
* Экспорт палитры в файл - экспортирует палитру в ini файл. 

#### Профиль 

![Профиль](/doc/menu_profile.png)

* Перерисовать - перерисовывает профили, на основе существующих настроек.
* Увеличить - увеличивает масштаб.
* Уменьшить - уменьшает масштаб.
* Оригинальный размер - возвращает масштаб в исходный.
* К размеру окна - уменьшает/увеличивает изображение так, что бы оно уместилось в размеры окна.
* Экспортировать изображение - экспортирует построения в графический файл.

#### Срез 

![Срез](/doc/menu_cut.png)

* Перерисовать - перерисовывает срез, на основе существующих настроек.
* Увеличить - увеличивает масштаб.
* Уменьшить - уменьшает масштаб.
* Оригинальный размер - возвращает масштаб в исходный.
* К размеру окна - уменьшает/увеличивает изображение так, что бы оно уместилось в размеры окна.
* Экспортировать изображение - экспортирует построения в графический файл.

#### Справка

![Справка](/doc/menu_help.png)

* Справка - вызвать справочное руководство.
* О Qt - информация о библиотеке.
* О программе - краткая информация о программе.


### Панель настроек

Здесь располагаются параметры настройки проекта и отображения результатов построения.

#### Вкладка Настройки

![Вкладка Настройки](/doc/settings.png)

Отображает текущие исходные параметры прототипа пробной площади. Содержит ряд параметров, определяющих графическую конфигурацию прототипа. 

*Топологический чертеж (Layouts):*  
Позволяет задавать наличие/отсутствие основных компонентов, отображаемых на рабочей области: Разметка, Стволы, Кроны).

*Сортировка стволов (Trunks sorting):*  
Устаревшее. Сейчас не используется.

*Ограничения (Restrictions):*  
Позволяет отображение деревьев по выбранным параметрам: по Высоте (отображаются деревья от и до заданной высоты). и по Диаметру (отображаются деревья от и до заданного диаметра ствола).. При отсутствии отметок данные ограничения игнорируются и отображаются все деревья из Таблицы данных).

*Масштаб (Scale):*  
Позволяет задавать разрешение графического элемента.

*Глубина среза (Cut):*  
Позволяет отображать глубину среза при построении Профильной диаграммы, при этом отображаются деревья, попавшие в границы от Линии среза (После нажатия вкладки Срез линия, автоматически строящаяся через точку поставленную в любом месте Графического окна) до величины глубины среза с обеих сторон)

#### Вкладка Площадь

![Вкладка Площадь](/doc/plate.png)

Позволяет отображать качественно-количественные показатели прототипа пробной площади.

*Вероятный размер пробной площади:*  
Позволяет задавать ширину (X) и высоту (Y) прототипа пробной площади, а также цвет подложки, нажав на вкладку Цвет (в качестве способа заливки можно выбирать как цвет, так и….) 

*Область вокруг пробной площади:*  
Позволяет провести дополнительную границу вокруг пробной площади заданной ширины, а также очистить от крон область между этой и основной границей

*Граница:*  
Позволяет изменять толщину и цвет границы прототипа пробной площади 

*Отметки на границе:*  
Позволяет отображать/не отображать отметки на границах пробной площади. При этом возможно задавать/не задавать числовые отметки, с задаваемым интервалом, а также при помощи специальной вкладки менять качественно-количественные показатели шрифта) 

*Сетка:*  
Позволяет отображать/не отображать основные и вспомогательные линии сетки. Также позволяет задавать индивидуально цвет линий и ширину шага.


#### Вкладка Стволы

![Вкладка Стволы](/doc/trunks.png)


Позволяет изменять графические параметры отображаемых стволов деревьев.

*Круг:*   
При активации данной вкладки графический образ сечения ствола на Графическом окне отображается в виде круга

*Согласно палитре:*  
Позволяет отменить прорисовку прототипа поперечного среза ствола деревьев. Не реализовано, при выборе стволы не обрисовываются.

*Размер:*  
Позволяет изменять размер диаметра ствола. При этом в Графическом окне возможно отображение как реального размера диаметра ствола из Исходной таблицы данных (в соответствие с масштабом), либо задать условный размер диаметра ствола (одинаковый для всех деревьев).

*Нумерация:*  
Позволяет отображать/не отображать нумерацию стволов (номер дерева автоматически определяется из графы «№» в Исходной таблице данных. При этом возможно изменение параметров шрифта, используя вкладку «Шрифт».

#### Вкладка Кроны

![Вкладка Кроны](/doc/crowns.png)

Позволяет изменять графические параметры отображаемых крон деревьев. Содержит вкладки: 

*Узел:*  
Позволяет отображать/не отображать точки, по которым строилась крона (точки автоматически определяются из Исходной таблицы данных. Возможно изменять размер отметок.

*Прозрачность:*  
Позволяет изменять прозрачность заливки крон, используя условные значения от 0 (заливка крон отсутствует) до 254 (максимальная плотность заливки). При наложении крон одна на другую, значения величины плотности общей области автоматически суммируется.


### Рабочая область

Здесь происходит отображение результатов построений, производятся замеры расстояний и площадей. Кроме того здесь производятся манипуляции с данными и палитрой, ведется журнал событий системы.

#### Вкладка Профиль

![Вкладка Профиль](/doc/profile.png)

Отображает область построения профиля. Тут же можно производить замеры дистанций и площадей.
К основным функциям можно получить доступ через пункты [главного меню](#меню) и через панель инструментов:

![Панель инструментов профиля](/doc/profile_panel.png)

* *"Перерисовка"* - Позволяет перерисовку графического образа в соответствии с измененными параметрами.
* *"-"*, *"+"* - Позволяет уменьшение/увеличение размера графического образа пробной площади.
* *"1:1"* - Возвращает масштаб к исходному. «Подгонка» - Приводит масштаб к такому виду, что бы вся площадь поместилась на экране.
* *"М"* - Позволяет производить измерение расстояния между двумя произвольными точками (в данном масштабе). Для этого необходимо активировать данную кнопку, нажав на нее левой кнопкой мыши и переведя курсор на Графическое окно, единожды нажав в двух произвольных точках. При этом ниже кнопки Перерисовка появится значение дистанции между этими точками (согласно масштабу).
* *"A"* - Позволяет производить измерения площадей (в данном масштабе). Для этого необходимо активировать данную кнопку, нажав на нее левой кнопкой мыши и переведя курсор на Графическое окно, щелкая мышкой грубо обрамить измеряемую площадь, для замыкания площади щелкнуть правой кнопкой мыши. После чего передвигая узлы точно обрамить измеряемую площадь. При этом ниже вкладки Перерисовка появится значение площади образованной фигуры (согласно масштабу).
* *"Срез"* - Кнопка необходима для определения точки, через которую пройдет линия среза (параллельно оси Х), при построении Профильной диаграммы. Для определения точки необходимо однократно нажать на данную вкладку и на области Графическое окно также однократно нажать левой кнопкой мыши. Затем, для просмотра результата построения профильной диаграммы необходимо нажать на панель инструментов Срез из Правой группы панелей инструментов.

В области рисования, отрисовывается пробная площадь, на основании установленных настроек и исходных данных. При наведении курсора мыши на любое дерево будет выведена краткая подсказка, отражающие его основные параметры.


#### Вкладка Срез

![Вкладка Срез](/doc/cut.png)

Рисует профильную диаграмму. Отрисовывается только после активизации кнопки "Cut" на вкладке [Профиль](#вкладка-профиль).  
Панель инструментов выглядит аналогично вкладке [Профиль](#вкладка-профиль)

![Панель инструментов Среза](/doc/cut_panel.png)

за исключением функций замеров, в остальном значение кнопок аналогично.

#### Вкладка Данные

![Вкладка Данные](/doc/data.png)

Отображает таблицу данных. Данные можно импортировать из файла, можно вводить вручную, для чего достаточно нажать кнопку "Ins" на клавиатуре, после чего появится [диалог добавления/изменения данных](#окно-добавленияредактирование-данных). Для редактирования достаточно дважды кликнуть в нужной строке.

#### Вкладка Палитра

![Вкладка Палитра](/doc/pallete.png)

На этой вкладке отображается используемая палитра. Для добавления новых записей достаточно воспользоваться соответствующим пунктом [меню](#палитра) или нажать на клавиатуре кнопку `Ins`, после чего будет вызван [диалог добавления/редактирования палитры](#окно-добавленияредактирования-палитры). Для изменения записи достаточно дважды щелкнуть мышкой в нужной строке. Кроме того, меню [Палитра](#палитра) предоставляет дополнительные возможности манипулирования с палитрой.

#### Вкладка Журнал

![Вкладка Журнал](/doc/log.png)

Отображает события системы. Для очистки достаточно нажать кнопку "Очистить"


### Окно добавления/редактирование данных

![Окно добавления и редактирования данных](/doc/data_add_edit.png)

Открывается при добавлении и редактировании данных.

### Окно добавления/редактирования палитры

![Окно добавления и редактирования палитры](/doc/pallete_add_edit.png)

Открывается при добавлении новых и редактировании старых данных в палитре.

Запись в палитре определяется следующими параметрами:

*Показ*  
Позволяет отображать/не отображать деревья выбранной породы.

*Имя*  
Позволяет вводить видовое название деревьев, которое в последующем будет отражаться во Всплывающем окне.

*Цвет ствола*  
Позволяет менять цвет ствола.

*Заполнение кроны*  
Позволяет отображать/не отображать цветовую заливку графического образа кроны. Отдельная вкладка позволяет менять тип заливки графического образа кроны (14 типов).

*Граница кроны*  
Позволяет менять толщину и цвет границы графического образа кроны дерева.

*Тип дерева*  
Позволяет определить тип дерева (лиственное/хвойное). При построении Профильной диаграммы крона лиственных деревьев отражается в виде полукруга, а хвойных - в виде треугольника. 


## Горячие клавиши

| Клавиша | Область применения | Описание                                                 |
| ------- | ------------------ | -------------------------------------------------------- |
| Ins     | Вкладка Данные     | Вызывает диалог добавление новой строки в таблицу данных |
|         | Вкладка Палитра    | Вызывает диалог добавления новой строки в палитру        |
| F5      | Глобально          | Перерисовывает отображение проекций и среза              |


## Исходная таблица данных 

**Внимание!!!**  
В качестве [Исходной таблицы данных](/doc/data_table.md) для работы в программе Crowns используется **только** текстовые файлы (Текст MS-DOS). Возможен перевод вручную файлов MS Excel и OpenOffice Calc.

**Внимание!!!**  
В качестве разделителя десятичной дроби программа Crowns воспринимает **только точки** (например, если при переводе файла из MS Excel или OpenOffice Calc в текстовый формат дробное число в таблице данных имело вид 3,14, то перед использованием данного файла в программе *Crowns* необходимо, при помощи любого текстового редактора, через **меню замена** заменить все запятые точками таким образом, чтобы число приобрело вид: `3.14`).

**Внимание!!!**  
Число **столбцов** в исходной таблице данных должно быть строго определенным, а также построение данных идет лишь с **третьей строки!** (первые две строчки игнорируются, оставляются в распоряжение пользователя, например разместить заголовки и комментарий к данным). 

[Посмотреть образец таблицы данных.](/doc/data_table.md)


